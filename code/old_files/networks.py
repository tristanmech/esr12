import numpy as np
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from funcs import set_rc_params
set_rc_params()
from keras.models import Sequential
from keras.layers.core import Dense
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from keras.optimizers import adam,adadelta,sgd,rmsprop
import keras
from keras import models
from keras import layers

#np.random.seed(42)
initializer = keras.initializers.he_normal(seed=42)
def regression_model():
    model = Sequential()
    model.add(Dense(64, input_dim=6, activation='relu'))
    model.add(Dense(64, activation='relu'))
    optim = adam(lr=0.01,decay=0.001)
#    optim = rmsprop()
    model.add(Dense(1,activation="sigmoid"))
    model.compile(loss='mae', optimizer=optim, metrics=['mse'])
    return model

def classification_model():
    model = Sequential()
    model.add(Dense(16, input_dim=3,kernel_initializer=initializer, activation='relu'))
    model.add(Dense(16, activation='relu'))
    model.add(Dense(16, activation='relu'))
#    model.add(Dense(4, activation='relu'))
#    optim = adam(lr=0.00001)
    optim = rmsprop(lr=0.0001,decay=0.)
#    model.add(Dense(1,activation="linear"))
    model.add(Dense(1, activation="sigmoid"))
    model.compile(loss="binary_crossentropy", optimizer=optim, metrics=['mse',"accuracy"])
    return model

def build_model():
    model = models.Sequential()
    model.add(layers.Dense(64, activation='relu',input_dim=3))
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dense(1))
    model.compile(optimizer='adam', loss='mse', metrics=['mae'])
    return model
import numpy as np
import matplotlib.pyplot as plt
##################### FAKE DATA ####################
from funcs import load_fake_data
station_positions,station_velocities,station_index,stations,sat_positions,sat_velocities,satellite_index,satellites,debris_positions,debris_velocities,debris_index,debris,sat_positions_new,sat_velocities_new,satellite_index_new,satellites_new = load_fake_data()

#%%
#################### CLOSE ENCOUNTERS DETECTION ####################
from funcs import encounter_detection
# Their paper had 10m = 0.01 km
miss_dist = 10 #km

time_indexes_new,primary_index_new,secondary_index_new,distances_new = encounter_detection(miss_dist,station_positions,debris_positions)

print(f"\n\n{len(primary_index_new)} station-debris encounters with less than {miss_dist}km distance detected")
#%%
from funcs import get_names
names_stations = get_names(primary_index_new,station_index)
names_new = get_names(secondary_index_new,debris_index)

from skyfield.api import load
ts = load.timescale()
hours = np.arange(0.0,24,0.05)
times = ts.utc(2019,month=6,day=15,hour=hours)

times_of_encounter_new = np.asarray(time_indexes_new)
times_of_encounter_new = np.reshape(times_of_encounter_new,(times_of_encounter_new.shape[0]))

distances_new = np.asarray(distances_new)
dists_new = []
for i,j in enumerate(time_indexes_new):
    dists_new.append(distances_new[i,j])
dists_new = np.asarray(dists_new)
dists_new = np.reshape(dists_new,(dists_new.shape[0]))

encounters_stations = np.array([names_stations,names_new,times_of_encounter_new,dists_new]).T

from funcs import more_than_one_encounters_mitigation
encounters_stations = more_than_one_encounters_mitigation(encounters_stations)
#%%
# Get the position and velocity for each object involved in an
# encounter
from funcs import load_FAKE_velocities,load_FAKE_positions,load_FAKE_indexes
# Velocities - km/s
FAKE_station_velocities,FAKE_sat_velocities,FAKE_debris_velocities,FAKE_sat_velocities_new = load_FAKE_velocities()
# Positions - km
FAKE_station_positions,FAKE_sat_positions,FAKE_debris_positions,FAKE_sat_positions_new = load_FAKE_positions()

# For both positions and velocities the arrays are (timesteps,xyz)
# Use the indexes to find the correct xyz value for each object at
# each time step
FAKE_station_index,FAKE_satellite_index,FAKE_debris_index,FAKE_satellite_index_new = load_FAKE_indexes()
#%%
from funcs import vel_and_pos_at_encounter

station_encounter_positions,station_encounter_velocities,station_debris_encounter_positions,station_debris_encounter_velocities = vel_and_pos_at_encounter(encounters_stations,FAKE_station_index,FAKE_debris_index,FAKE_station_positions,FAKE_station_velocities,FAKE_debris_positions,FAKE_debris_velocities)
#%%
# Relative velocities
# v_rel = v_sec - v_prim
rel_vel_stations = station_debris_encounter_velocities - station_encounter_velocities
# Relative positions
# p_rel = p_sec - p_prim
rel_pos_stations = station_debris_encounter_positions - station_encounter_positions

from funcs import transformation_matrix,basis_vectors
# Basis vectors for each object
# Stations
ex_stations,ey_stations,ez_stations = basis_vectors(rel_vel_stations,rel_pos_stations)
#%%
# Transformations
# Stations
new_rel_vel_stations = np.zeros(3)
new_rel_pos_stations = np.zeros(3)
for vel_vec,pos_vec,ex,ey,ez in zip(rel_vel_stations,rel_pos_stations,ex_stations,ey_stations,ez_stations):
    new_rel_vel_stations = np.vstack((new_rel_vel_stations,transformation_matrix(ex,ey,ez,vel_vec)))
    new_rel_pos_stations = np.vstack((new_rel_pos_stations,transformation_matrix(ex,ey,ez,pos_vec)))
# remove the padding
new_rel_vel_stations= np.delete(new_rel_vel_stations,np.s_[0],axis=0)
new_rel_pos_stations= np.delete(new_rel_pos_stations,np.s_[0],axis=0)
#%%
from funcs import probability_of_collision
theta = 0 # degrees of rotation to new coordinates. It is alway negative so I
# leave it like that and put the minus at ym like they do in the paper
r_p_sat = 9 # in km
r_s_sat = 1
R = r_p_sat + r_s_sat
r_p_station = 9 # in km
r_s_station = 1
R_station = r_p_station + r_s_station

N = 20
sigma_x = 10 # km
sigma_y = sigma_x

PC_stations = probability_of_collision(N,R_station,sigma_x,sigma_y,theta,new_rel_pos_stations)

#PC_stations = np.round(np.asarray(PC_stations),decimals=15)
#%%
from funcs import extract_OE
FAKE_stations = np.load("FAKE_DATA/FAKE_stations.npy",allow_pickle=True)
FAKE_satellites = np.load("FAKE_DATA/FAKE_satellites.npy",allow_pickle=True)
FAKE_debris = np.load("FAKE_DATA/FAKE_debris.npy",allow_pickle=True)
FAKE_satellites_new = np.load("FAKE_DATA/FAKE_satellites_new.npy",allow_pickle=True)

OE_stations_pr,OE_stations_sec = extract_OE(encounters_stations,FAKE_stations,FAKE_debris)
# There is a problem with the fiddled satellites, they have only positions and
# velocities fiddled, not their OE which makes the following line useless
#OE_sat_pr,OE_sat_sec = extract_OE(encounters_satellites,FAKE_satellites,FAKE_satellites_new)

OE_stations_pr = OE_stations_pr.astype(float)
OE_stations_sec = OE_stations_sec.astype(float)

ecc_train_stations = np.abs(OE_stations_pr.T[0] - OE_stations_sec.T[0])
inc_train_stations = np.abs(OE_stations_pr.T[1] - OE_stations_sec.T[1])
OMEGA_train_stations = np.abs(OE_stations_pr.T[2] - OE_stations_sec.T[2])
omega_train_stations = np.abs(OE_stations_pr.T[3] - OE_stations_sec.T[3])
mean_anomaly_train_stations = np.abs(OE_stations_pr.T[4] - OE_stations_sec.T[4])
mean_motion_train_stations = np.abs(OE_stations_pr.T[5] - OE_stations_sec.T[5])

X_train_st = np.column_stack((ecc_train_stations.T,inc_train_stations.T,OMEGA_train_stations.T,omega_train_stations.T,mean_anomaly_train_stations.T,mean_motion_train_stations.T))

Y_train_stations = np.round(np.asarray(PC_stations),decimals=15)
#%%
np.save("AI_DATA/X_train_st",X_train_st)
np.save("AI_DATA/Y_train_stations",Y_train_stations)
import numpy as np
import matplotlib.pyplot as plt
from funcs import set_rc_params,plot_precision_recall_vs_threshold,plot_roc_curve
set_rc_params()
import seaborn as sns

Y_train_stations = np.load("AI_DATA/Y_train_stations.npy",allow_pickle=True)
Y_train_sat = np.load("AI_DATA/Y_train_sat.npy",allow_pickle=True)
X_train_st = np.load("AI_DATA/X_train_st.npy",allow_pickle=True)
X_train_sat = np.load("AI_DATA/X_train_sat.npy",allow_pickle=True)

X = np.vstack((X_train_st,X_train_sat))
Y = np.hstack((Y_train_stations,Y_train_sat))

X_max = np.max(X,axis=0)
for i in X:
    i[1] = i[1]/X_max[1]
    i[2] = i[2]/X_max[2]
    i[4] = i[4]/X_max[4]
    i[5] = i[5]/X_max[5]

# Delete the ecc,omega and mean_motion collumns
X = np.delete(X,(0),axis=1)
X = np.delete(X,(2),axis=1)
X = np.delete(X,(3),axis=1)

from sklearn.model_selection import train_test_split
X, X_test, Y, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
#%%
threshold2 = 0.001
zero_indx = np.where(Y<threshold2)
one_indx = np.where(Y>=threshold2)
zero_indx_test = np.where(Y_test<threshold2)
one_indx_test = np.where(Y_test>=threshold2)
# For the multiclass
X_reg = X[one_indx]
Y_reg = Y[one_indx]
X_reg_test = X_test[one_indx_test]
Y_reg_test = Y_test[one_indx_test]
#%%
from sklearn.preprocessing import PolynomialFeatures
poly_feat = PolynomialFeatures(degree=3)
X_reg = poly_feat.fit_transform(X_reg)
X_reg_test = poly_feat.fit_transform(X_reg_test)

from networks import regression_model
from keras.callbacks import EarlyStopping
from keras.datasets import boston_housing

(train_data, train_targets), (test_data, test_targets) = boston_housing.load_data()
mean = train_data.mean(axis=0)
train_data -= mean
std = train_data.std(axis=0)
train_data /= std
test_data -= mean
test_data /= std

from networks import build_model
model=build_model()
model.fit(train_data,train_targets,epochs=40,verbose=1)
model.predict(test_data)
sns.distplot(model.predict(test_data),kde=False,bins=15)
sns.distplot(test_targets,kde=False,bins=15)
#k = 4
#num_val_samples = len(X_reg) // k
#num_epochs = 100
#all_scores = []
#
#for i in range(k):
#    print('processing fold #', i)
#    val_data = X_reg[i * num_val_samples: (i + 1) * num_val_samples]
#    val_targets = Y_reg[i * num_val_samples: (i + 1) * num_val_samples]
#    partial_X_reg = np.concatenate([X_reg[:i * num_val_samples],
#                                    X_reg[(i + 1) * num_val_samples:]],axis=0)
#    
#    partial_Y_reg = np.concatenate([Y_reg[:i * num_val_samples],
#                                    Y_reg[(i + 1) * num_val_samples:]],axis=0)
#    model = regression_model()
#    model.fit(partial_X_reg, partial_Y_reg,epochs=num_epochs, batch_size=32, verbose=1)
#    val_mse, val_mae = model.evaluate(val_data, val_targets, verbose=1)
#    all_scores.append(val_mae)

#model = regression_model()
model.fit(X_reg,Y_reg,batch_size=16,verbose=1,epochs=50)
#test_mse_score, test_mae_score = model.evaluate(X_reg_test,Y_reg_test)
#
preds = model.predict(X_reg_test)
#
sns.distplot(preds,kde=False,bins=15)
sns.distplot(Y_reg_test,kde=False,bins=15)

import numpy as np
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from funcs import set_rc_params,plot_precision_recall_vs_threshold,plot_roc_curve,array2bmatrix
set_rc_params()

Y_train_stations = np.load("AI_DATA/Y_train_stations.npy",allow_pickle=True)
Y_train_sat = np.load("AI_DATA/Y_train_sat.npy",allow_pickle=True)
X_train_st = np.load("AI_DATA/X_train_st.npy",allow_pickle=True)
X_train_sat = np.load("AI_DATA/X_train_sat.npy",allow_pickle=True)

X = np.vstack((X_train_st,X_train_sat))
Y = np.hstack((Y_train_stations,Y_train_sat))

X_max = np.max(X,axis=0)
for i in X:
    i[1] = i[1]/X_max[1]
    i[2] = i[2]/X_max[2]
    i[4] = i[4]/X_max[4]
    i[5] = i[5]/X_max[5]
X = np.delete(X,(0),axis=1)
X = np.delete(X,(2),axis=1)
X = np.delete(X,(3),axis=1)


from sklearn.model_selection import train_test_split
X, X_test, Y, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

#from sklearn.preprocessing import StandardScaler
#scaler = StandardScaler()
#X = scaler.fit_transform(X)
#X_test = scaler.fit_transform(X_test)

#%%
# Robot 5 will be a multiclass version so we will define some thresholds and
# assign each probability to a different class
# Since the dataset is already massively imbalanced maybe try this with the _reg
# sets
threshold2 = 0.001
zero_indx = np.where(Y<threshold2)
one_indx = np.where(Y>=threshold2)
zero_indx_test = np.where(Y_test<threshold2)
one_indx_test = np.where(Y_test>=threshold2)
# For the multiclass
X_mult = X[one_indx]
Y_mult = Y[one_indx]
X_mult_test = X_test[one_indx_test]
Y_mult_test = Y_test[one_indx_test]
# Multiclass assignment, lets create 4 classes
class_thresh = np.linspace(threshold2,0.4,4)
cl1 = np.where((Y_mult>=threshold2) & (Y_mult<class_thresh[1]))
cl2 = np.where((Y_mult>=class_thresh[1]) & (Y_mult<class_thresh[2]))
cl3 = np.where(Y_mult>=class_thresh[2])

Y_mult[cl1] = 0
Y_mult[cl2] = 1
Y_mult[cl3] = 2
Y_mult_test[(Y_mult_test>=threshold2) & (Y_mult_test<0.134)] = 0
Y_mult_test[(Y_mult_test>=0.134) & (Y_mult_test<0.267)] = 1
Y_mult_test[(Y_mult_test>=class_thresh[2]) & (Y_mult_test<1)] = 2
# For the classifier. The Y data now will become only 0s and 1s and the X data
# are the same as will the test data
#Y[zero_indx] = 0
#Y[one_indx] = 1
#Y_test[zero_indx_test] = 0
#Y_test[one_indx_test] = 1

from imblearn.over_sampling import SMOTE, ADASYN
from imblearn.under_sampling import RandomUnderSampler
underpasta = RandomUnderSampler(random_state=42)
pasta = ADASYN(random_state=42)
X_resampled, Y_resampled = pasta.fit_resample(X_mult, Y_mult)
#X_resampled, Y_resampled = X_mult,Y_mult
#%%
from sklearn.model_selection import cross_val_score,cross_val_predict,cross_validate,KFold,RandomizedSearchCV
from sklearn.metrics import confusion_matrix,precision_score, recall_score,f1_score,precision_recall_curve,roc_curve, roc_auc_score,auc
kfold = KFold(n_splits=10, shuffle=True, random_state=42)


from sklearn.neural_network import MLPClassifier

nn = MLPClassifier(random_state=42,activation="relu",hidden_layer_sizes=(10000,),validation_fraction=0.1,solver="sgd",verbose=1,max_iter=10000000,learning_rate = "adaptive",optimizer=learning_rate_init=0.01,batch_size=12,tol=0.00001)
nn.fit(X_mult,Y_mult)

#%%
import seaborn as sns
plt.figure()
sns.distplot(nn.predict(X_mult_test),kde=False,bins=15,color="C0",label="Predictions")
sns.distplot(Y_mult_test,kde=False,bins=15,color="C2",label="True labels")
plt.legend()
plt.savefig("figures/RandomForest/multiclass_predictions",bbox_inches="tight")

conf_mat_train = confusion_matrix(Y_resampled,nn.predict(X_resampled))
print(array2bmatrix(conf_mat_train))
#plt.matshow(confusion_matrix(Y_resampled,nn.predict(X_resampled)), cmap=plt.cm.gray)
conf_mat_test = confusion_matrix(Y_mult_test,nn.predict(X_mult_test))
print(array2bmatrix(conf_mat_test))
#plt.matshow(confusion_matrix(Y_mult_test,nn.predict(X_mult_test)), cmap=plt.cm.gray)
#%%
#https://stackoverflow.com/questions/45332410/sklearn-roc-for-multiclass-classification
# Compute ROC curve and ROC area for each class
from sklearn.multiclass import OneVsRestClassifier
from sklearn.preprocessing import label_binarize
clf = OneVsRestClassifier(nn)
y = label_binarize(Y_resampled, classes=[0,1,2])
y_test = label_binarize(Y_mult_test, classes=[0,1,2])
y_score = clf.fit(X_resampled,y).predict_proba(X_mult_test)

n_classes = 3
fpr = dict()
tpr = dict()
roc_auc = dict()
for i in range(n_classes):
    fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
    roc_auc[i] = auc(fpr[i], tpr[i])

# Plot of a ROC curve for a specific class
for i in range(n_classes):
    plt.figure()
    plt.plot(fpr[i], tpr[i], label='ROC curve (area = %0.2f)' % roc_auc[i])
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
#    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()


























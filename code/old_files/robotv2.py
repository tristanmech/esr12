import numpy as np
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from funcs import set_rc_params
set_rc_params()

Y_train_stations = np.load("AI_DATA/Y_train_stations.npy",allow_pickle=True)
Y_train_sat = np.load("AI_DATA/Y_train_sat.npy",allow_pickle=True)
X_train_st = np.load("AI_DATA/X_train_st.npy",allow_pickle=True)
X_train_sat = np.load("AI_DATA/X_train_sat.npy",allow_pickle=True)

test_ratio = 0.15
num_test = int(test_ratio*(X_train_sat.shape[0]+X_train_st.shape[0]))

X = np.vstack((X_train_st,X_train_sat[:-num_test,:]))
Y = np.hstack((Y_train_stations,Y_train_sat[:-num_test]))

# NORMALIZE
X_max = np.max(X,axis=0)
for i in X:
    i[1] = i[1]/X_max[1]
    i[2] = i[2]/X_max[2]
    i[4] = i[4]/X_max[4]
    i[5] = i[5]/X_max[5]

# Delete the ecc,omega and mean_motion collumns
X = np.delete(X,(0),axis=1)
X = np.delete(X,(2),axis=1)
X = np.delete(X,(3),axis=1)

X_test = X_train_sat[-num_test:]
X_test = np.delete(X_test,(0),axis=1)
X_test = np.delete(X_test,(2),axis=1)
X_test = np.delete(X_test,(3),axis=1)
X_max2 = np.max(X_test,axis=0)
for i in X_test:
    i[0] = i[0]/X_max2[0]
    i[1] = i[1]/X_max2[1]
    i[2] = i[2]/X_max2[2]
Y_test = Y_train_sat[-num_test:]

# Remove the zeroes
#X = X[Y>0]
#X = X
#Y = Y[Y>0]

#X_test = X_test[Y_test>0]
#X_test = X_test
#Y_test = Y_test[Y_test>0]
#%%
# Idea 2: Still two networks but now one is a classifier and one a regressor
# Net1 will determine if the probability of collision is less than a threshold
# Class 1: The probability is less than the threshold
# Class 0: The probability is more than the threshold --> use Net2 to see how
# much
threshold2 = 0.001
zero_indx = np.where(Y<threshold2)
one_indx = np.where(Y>=threshold2)
zero_indx_test = np.where(Y_test<threshold2)
one_indx_test = np.where(Y_test>=threshold2)
# For the regressor
X_reg = X[one_indx]
Y_reg = Y[one_indx]
X_reg_test = X_test[one_indx_test]
Y_reg_test = Y_test[one_indx_test]

# For the classifier. The Y data now will become only 0s and 1s and the X data
# are the same as will the test data
Y[zero_indx] = 0
Y[one_indx] = 1
Y_test[zero_indx_test] = 0
Y_test[one_indx_test] = 1

from imblearn.over_sampling import SMOTE, ADASYN
pasta = SMOTE(random_state=42)
X_resampled, Y_resampled = pasta.fit_resample(X, Y)
#%%
# SVM for classification

# The oversampling seems to have fixed the classification issue
#X_resampled = X
#Y_resampled = Y

# COMPARISON OF METHODS
# SGD, SVC, RandomForest

from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier

sgd_clf = SGDClassifier(random_state=42)
sgd_clf.fit(X_resampled,Y_resampled)
preds = sgd_clf.predict(X_test)
print(f"SGD score: {sgd_clf.score(X_test,Y_test)}")
svc = SVC(random_state=42, gamma="auto",kernel="poly",degree=5)
#svc = SVC(random_state=42,C=100,gamma=5)
svc.fit(X_resampled,Y_resampled)
preds_SVM = svc.predict(X_test)
print(f"SVM score: {svc.score(X_test,Y_test)}")

from sklearn.model_selection import cross_val_score,cross_val_predict,cross_validate,KFold
kfold = KFold(n_splits=10, shuffle=True, random_state=42)
cross_val_score(sgd_clf, X_resampled, Y_resampled, cv=kfold, scoring="accuracy")
cross_val_score(svc, X_resampled, Y_resampled, cv=kfold, scoring="accuracy")

y_train_pred_sgd = cross_val_predict(sgd_clf, X_resampled, Y_resampled, cv=kfold)
y_train_pred_svc = cross_val_predict(svc, X_resampled, Y_resampled, cv=kfold)

from sklearn.metrics import confusion_matrix,precision_score, recall_score,f1_score
confusion_matrix(Y_resampled,y_train_pred_sgd)
confusion_matrix(Y_resampled,y_train_pred_svc)

recall_score(Y_resampled,y_train_pred_sgd)
recall_score(Y_resampled,y_train_pred_svc)

from sklearn.metrics import precision_recall_curve
y_scores = cross_val_predict(sgd_clf, X_resampled, Y_resampled, cv=kfold,method="decision_function")
y_scores_svc = cross_val_predict(svc, X_resampled, Y_resampled, cv=kfold,method="decision_function")

precisions, recalls, thresholds = precision_recall_curve(Y_resampled, y_scores)
precisions2, recalls2, thresholds2 = precision_recall_curve(Y_resampled, y_scores_svc)
def plot_precision_recall_vs_threshold(precisions, recalls, thresholds):
    plt.plot(thresholds, precisions[:-1], "b--", label="Precision")
    plt.plot(thresholds, recalls[:-1], "g-", label="Recall")
    plt.xlabel("Threshold")
    plt.legend(loc="upper left")
    plt.ylim([0, 1])
plot_precision_recall_vs_threshold(precisions, recalls, thresholds)
plt.show()

y_train_pred_sgd90 = (y_scores > -0.92)
recall_score(Y_resampled, y_train_pred_sgd90)
confusion_matrix(Y_resampled,y_train_pred_sgd90)
# Now we have increased the recall to 98% so we have lots of false positives
# but we have very few false negatives

from sklearn.metrics import roc_curve, roc_auc_score
fpr, tpr, thresholds = roc_curve(Y_resampled, y_scores)
fpr2, tpr2, thresholds2 = roc_curve(Y_resampled, y_scores_svc)

roc_auc_score(Y_resampled, y_scores)

def plot_roc_curve(fpr, tpr, label=None):
    plt.plot(fpr, tpr, linewidth=2, label=label)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.axis([0, 1, 0, 1])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
plot_roc_curve(fpr, tpr)
plt.show()

# Random forest
from sklearn.ensemble import RandomForestClassifier
forest_clf = RandomForestClassifier(random_state=42,n_estimators=50)
y_probas_forest = cross_val_predict(forest_clf, X_resampled, Y_resampled, cv=kfold,method="predict_proba")

y_scores_forest = y_probas_forest[:, 1]
# score = proba of positive class
fpr_forest, tpr_forest, thresholds_forest = roc_curve(Y_resampled,y_scores_forest)
precisions3, recalls3, thresholds3 = precision_recall_curve(Y_resampled, y_scores_forest)
plot_precision_recall_vs_threshold(precisions3, recalls3, thresholds3)

y_train_pred_forest = cross_val_predict(forest_clf, X_resampled, Y_resampled, cv=kfold)
confusion_matrix(Y_resampled,y_train_pred_forest)

y_train_pred_forest85 = (y_scores_forest > 0.478) # keep the false stuff under 1k
recall_score(Y_resampled, y_train_pred_forest85)
confusion_matrix(Y_resampled,y_train_pred_forest85)

preds_RForest = forest_clf.predict_proba(X_test)
y_scores_forest = preds_RForest[:, 1]
preds_RForest_90 = (y_scores_forest > 0.478)
print(f"Random Forest score: {forest_clf.score(X_test,Y_test)}")
confusion_matrix(Y_test,preds_RForest_90)
# Comparison of ROC curve
plt.plot(fpr, tpr, "g:", label="SGD")
plt.plot(fpr2, tpr2, "r--", label="SVC")
plot_roc_curve(fpr_forest, tpr_forest, "Random Forest")
plt.legend(loc="lower right")


# At the test set SVC performs best


from sklearn.ensemble import VotingClassifier
# another ensemble method
voting_clf = VotingClassifier(
        estimators=[('RForest', forest_clf), ('SGD', sgd_clf), ('SVC', svc)],
        voting="hard")
voting_clf.fit(X_resampled,Y_resampled)

from sklearn.metrics import accuracy_score
for clf in (forest_clf, sgd_clf, svc, voting_clf):
    clf.fit(X_resampled,Y_resampled)
    y_pred = clf.predict(X_test)
    print(clf.__class__.__name__, accuracy_score(Y_test, y_pred))



#from sklearn.model_selection import GridSearchCV
#param_grid = [{'C': list(np.linspace(10,200,20)), "gamma":list(np.linspace(1,10,10)),
# "degree":[3,4,5,6]}]
#svc2 = SVC(random_state=42)
#grid_search = GridSearchCV(svc2,param_grid,cv=kfold)

#grid_search.fit(X_resampled,Y_resampled)

#%%
from networks import classification_model
from keras.callbacks import EarlyStopping
from sklearn.model_selection import KFold,cross_val_score
from keras.wrappers.scikit_learn import KerasClassifier
#
early_stopping = EarlyStopping(monitor='val_acc', patience=5)
##seed = 1
##np.random.seed(seed)
#
## Classifier
classifier = classification_model()
fit_class = classifier.fit(X_resampled,Y_resampled,batch_size=4,epochs=500,verbose=1,validation_split=0.2,callbacks=[early_stopping])
#test_loss, test_acc = classifier.evaluate(X_test, Y_test)
#estimator = KerasClassifier(build_fn=classification_model, epochs=100, batch_size=32, verbose=0)sigmoid
#kfold = KFold(n_splits=10, shuffle=True, random_state=seed)
#results = cross_val_score(estimator, X, Y, cv=kfold)
#print('Test accuracy:', test_acc)
preds = classifier.predict(X_test)
#preds = classifier.predict(X_test)
preds == Y_test
plt.plot(fit_class.history["loss"])
plt.figure()
plt.plot(fit_class.history["acc"])
#%%
from networks import regression_model
#from keras.callbacks import EarlyStopping
# Regressor
#early_stopping = EarlyStopping(monitor='val_loss', patience=5)
regressor = regression_model()
fit_reg = regressor.fit(X_reg,Y_reg,batch_size=32,epochs=700,verbose=1,validation_split=0.15,
                        callbacks=[early_stopping])
preds_reg = regressor.predict(X_reg_test,batch_size=32)
#test_loss, test_acc = regressor.evaluate(X_reg_test, Y_reg_test)
#print('Test accuracy:', test_acc)

plt.plot(preds_reg,label="NN predictions")
plt.plot(Y_reg_test,label="THE TRUTH")
plt.legend()
plt.figure()
#plt.plot(fit_reg.history["loss"])
plt.show()

## Plot training & validation accuracy values
#plt.plot(fit_reg.history['acc'])
#plt.plot(fit_reg.history['val_acc'])
#plt.title('Model accuracy')
#plt.ylabel('Accuracy')
#plt.xlabel('Epoch')
#plt.legend(['Train', 'Test'], loc='upper left')
#plt.show()

# Plot training & validation loss values
plt.plot(fit_reg.history['loss'])
plt.plot(fit_reg.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper right')
plt.show()

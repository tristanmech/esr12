import numpy as np
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from funcs import set_rc_params,plot_precision_recall_vs_threshold,plot_roc_curve
set_rc_params()

Y_train_stations = np.load("AI_DATA/Y_train_stations.npy",allow_pickle=True)
Y_train_sat = np.load("AI_DATA/Y_train_sat.npy",allow_pickle=True)
X_train_st = np.load("AI_DATA/X_train_st.npy",allow_pickle=True)
X_train_sat = np.load("AI_DATA/X_train_sat.npy",allow_pickle=True)

X = np.vstack((X_train_st,X_train_sat))
Y = np.hstack((Y_train_stations,Y_train_sat))

X = np.delete(X,(0),axis=1)
X = np.delete(X,(2),axis=1)
X = np.delete(X,(3),axis=1)

from sklearn.model_selection import train_test_split
X, X_test, Y, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X = scaler.fit_transform(X)
X_test = scaler.fit_transform(X_test)

#%%
# Robot 5 will be a multiclass version so we will define some thresholds and
# assign each probability to a different class
# Since the dataset is already massively imbalanced maybe try this with the _reg
# sets
threshold2 = 0.001
zero_indx = np.where(Y<threshold2)
one_indx = np.where(Y>=threshold2)
zero_indx_test = np.where(Y_test<threshold2)
one_indx_test = np.where(Y_test>=threshold2)
# For the multiclass
X_mult = X[one_indx]
Y_mult = Y[one_indx]
X_mult_test = X_test[one_indx_test]
Y_mult_test = Y_test[one_indx_test]
# Multiclass assignment, lets create 4 classes
#class_thresh = np.linspace(threshold2,0.4,4)
#cl1 = np.where((Y_mult>=threshold2) & (Y_mult<class_thresh[1]))
#cl2 = np.where((Y_mult>=class_thresh[1]) & (Y_mult<class_thresh[2]))
#cl3 = np.where(Y_mult>=class_thresh[2])
#
#Y_mult[cl1] = 0
#Y_mult[cl2] = 1
#Y_mult[cl3] = 2
#Y_mult_test[(Y_mult_test>=threshold2) & (Y_mult_test<0.134)] = 0
#Y_mult_test[(Y_mult_test>=0.134) & (Y_mult_test<0.267)] = 1
#Y_mult_test[(Y_mult_test>=class_thresh[2]) & (Y_mult_test<1)] = 2
# For the classifier. The Y data now will become only 0s and 1s and the X data
# are the same as will the test data
Y[zero_indx] = 0
Y[one_indx] = 1
Y_test[zero_indx_test] = 0
Y_test[one_indx_test] = 1

from imblearn.over_sampling import SMOTE, ADASYN
from imblearn.under_sampling import RandomUnderSampler
underpasta = RandomUnderSampler(random_state=42)
pasta = SMOTE(random_state=42)
X_resampled, Y_resampled = pasta.fit_resample(X, Y)
#%%
from networks import classification_model
from keras.callbacks import EarlyStopping
from keras.wrappers.scikit_learn import KerasClassifier#
early_stopping = EarlyStopping(monitor='val_acc', patience=5)

from sklearn.model_selection import cross_val_score,cross_val_predict,cross_validate,KFold
from sklearn.metrics import confusion_matrix,precision_score, recall_score,f1_score,precision_recall_curve,roc_curve, roc_auc_score
kfold = KFold(n_splits=10, shuffle=True, random_state=42)


clf = classification_model()
clf.fit(X_resampled,Y_resampled,epochs=10,batch_size=64,validation_split=0.3,        verbose=1,callbacks=[early_stopping])
model = KerasClassifier(clf)
model.fit(X_resampled,Y_resampled)
cross_val_score(model,X_resampled,Y_resampled,cv=kfold,scoring="balanced_accuracy")

preds = clf.predict_classes(X_test)
confusion_matrix(Y_test,preds)




























import numpy as np
# We load them here to get them in array form
stations = np.load("data/stations.npy",allow_pickle=True)
satellites = np.load("data/satellites.npy",allow_pickle=True)
debris = np.load("CelesTrak_data/debris.npy",allow_pickle=True)

# STATIONS
ecc_stations = np.zeros((stations.shape[0],1))
inc_stations = np.zeros((stations.shape[0],1))
OMEGA_stations = np.zeros((stations.shape[0],1))
omega_stations = np.zeros((stations.shape[0],1))
mean_anomaly_stations = np.zeros((stations.shape[0],1))
mean_motion_stations = np.ones((stations.shape[0],1))*3
station_names = np.zeros((stations.shape[0],1),dtype=object)

for i,obj in enumerate(stations):
    OMEGA_stations[i] = np.math.degrees(obj.model.nodeo)
    inc_stations[i] = np.math.degrees(obj.model.inclo)
    mean_anomaly_stations[i] = np.math.degrees(obj.model.mo)
#    mean_motion_stations[i] = np.math.degrees(obj.model.no)
    station_names[i] = obj.name

# Stack them together and now each line is the orbital elements vector of an
# object. Add the name/satnum at the end for easier referece
OE_stations = np.hstack((ecc_stations,inc_stations,OMEGA_stations,
                         omega_stations,mean_anomaly_stations,
                         mean_motion_stations,station_names))


# SATELLITES
ecc_satellites = np.zeros((satellites.shape[0],1))
inc_satellites = np.zeros((satellites.shape[0],1))
OMEGA_satellites = np.zeros((satellites.shape[0],1))
omega_satellites = np.zeros((satellites.shape[0],1))
mean_anomaly_satellites = np.zeros((satellites.shape[0],1))
mean_motion_satellites = np.ones((satellites.shape[0],1))*3
satellite_names = np.zeros((satellites.shape[0],1),dtype=object)

for i,obj in enumerate(satellites):
    OMEGA_satellites[i] = np.math.degrees(obj.model.nodeo)
    inc_satellites[i] = np.math.degrees(obj.model.inclo)
    mean_anomaly_satellites[i] = np.math.degrees(obj.model.mo)
#    mean_motion_satellites[i] = np.math.degrees(obj.model.no)
    satellite_names[i] = obj.name

OE_satellites = np.hstack((ecc_satellites,inc_satellites,OMEGA_satellites,
                           omega_satellites,mean_anomaly_satellites,
                           mean_motion_satellites,satellite_names))

# DEBRIS
ecc_debris = np.zeros((debris.shape[0],1))
inc_debris = np.zeros((debris.shape[0],1))
OMEGA_debris = np.zeros((debris.shape[0],1))
omega_debris = np.zeros((debris.shape[0],1))
mean_anomaly_debris = np.zeros((debris.shape[0],1))
mean_motion_debris = np.ones((debris.shape[0],1))*3
debris_satnums = np.zeros((debris.shape[0],1),dtype=object)

for i,obj in enumerate(debris):
    OMEGA_debris[i] = np.math.degrees(obj.model.nodeo)
    inc_debris[i] = np.math.degrees(obj.model.inclo)
    mean_anomaly_debris[i] = np.math.degrees(obj.model.mo)
#    mean_motion_debris[i] = np.math.degrees(obj.model.no)
    debris_satnums[i] = obj.model.satnum

OE_debris = np.hstack((ecc_debris,inc_debris,OMEGA_debris,
                           omega_debris,mean_anomaly_debris,
                           mean_motion_debris,debris_satnums))

np.save("data/OE_stations",OE_stations)
np.save("data/OE_satellites",OE_satellites)
np.save("data/OE_debris",OE_debris)
#%%
# Lets fiddle with the data from the satellites
# Perturb each value of the OE with a random number drawn from a normal with

OE_satellites_new = np.hstack((ecc_satellites,inc_satellites,OMEGA_satellites,
                           omega_satellites,mean_anomaly_satellites,
                           mean_motion_satellites))

sat_fiddle = lambda a:np.random.normal(loc=a,scale=0.3*a)

for i,oe in enumerate(OE_satellites_new.T):
    OE_satellites_new.T[i] = sat_fiddle(oe)

from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt


#station_positions = np.load("positions/station_positions.npy")
positions = [station_positions,debris_positions]
#positions = [sat_positions]
fig = plt.figure()
ax = fig.gca(projection='3d')
#ax.plot(x, y, z)
#ax.plot(x1, y1, z1)
#for obj in positions:
#    print(obj)
#    cnt,i,j,k = 0,0,1,2
#    #while cnt < station_positions.shape[1]/3:
#    while cnt < 10:
#        x = obj[:,i]
#        y = obj[:,j]
#        z = obj[:,k]
#        i += 2
#        j += 2
#        k += 2
#        cnt += 1    
#        ax.plot(x,y,z,color="C0")
##ax.legend()
u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:20j]

x = 13000*np.cos(u)*np.sin(v)
y = 13000*np.sin(u)*np.sin(v)
z = 13000*np.cos(v)


#u = np.linspace(0, 2 * np.pi, 100)
#v = np.linspace(0, np.pi, 100)
#
#x = 13000 * np.outer(np.cos(u), np.sin(v))
#y = 13000 * np.outer(np.sin(u), np.sin(v))
#z = 13000 * np.outer(np.ones(np.size(u)), np.cos(v))
ax.plot_wireframe(x, y, z,color="C7")

cnt,i,j,k = 0,0,1,2
num = 5
#while cnt < station_positions.shape[1]/3:
obj = positions[0]
while cnt < num:
    x = obj[:,i]
    y = obj[:,j]
    z = obj[:,k]
    i += 2
    j += 2
    k += 2
    cnt += 1    
    ax.plot(x,y,z,color="C0",label="Station "+str(cnt))
#ax.legend()
obj = positions[1]
cnt,i,j,k = 0,0,1,2
#while cnt < station_positions.shape[1]/3:
while cnt < num:
    x = obj[:,i]
    y = obj[:,j]
    z = obj[:,k]
    i += 2
    j += 2
    k += 2
    cnt += 1    
    ax.plot(x,y,z,color="C1",label="Debris "+str(cnt))
#ax.legend()

plt.show()
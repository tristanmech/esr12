import numpy as np
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from funcs import set_rc_params,plot_precision_recall_vs_threshold,plot_roc_curve,array2bmatrix
set_rc_params()

Y_train_stations = np.load("AI_DATA/Y_train_stations.npy",allow_pickle=True)
Y_train_sat = np.load("AI_DATA/Y_train_sat.npy",allow_pickle=True)
X_train_st = np.load("AI_DATA/X_train_st.npy",allow_pickle=True)
X_train_sat = np.load("AI_DATA/X_train_sat.npy",allow_pickle=True)



X = np.vstack((X_train_st,X_train_sat))
Y = np.hstack((Y_train_stations,Y_train_sat))


# NORMALIZE
X_max = np.max(X,axis=0)
for i in X:
    i[1] = i[1]/X_max[1]
    i[2] = i[2]/X_max[2]
    i[4] = i[4]/X_max[4]
    i[5] = i[5]/X_max[5]

# Delete the ecc,omega and mean_motion collumns
X = np.delete(X,(0),axis=1)
X = np.delete(X,(2),axis=1)
X = np.delete(X,(3),axis=1)

#X = np.delete(X,((0),(1)),axis=1)
#X = np.delete(X,((4),(5)),axis=1)
#X = np.delete(X,((6),(7)),axis=1)
from sklearn.model_selection import train_test_split
X, X_test, Y, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
#from sklearn.preprocessing import StandardScaler
#scaler = StandardScaler()
#X = scaler.fit_transform(X)
#X_test = scaler.fit_transform(X_test)
#X_test = X_train_sat[-num_test:]
#X_test = np.delete(X_test,(0),axis=1)
#X_test = np.delete(X_test,(2),axis=1)
#X_test = np.delete(X_test,(3),axis=1)
# X_max2 = np.max(X_test,axis=0)
# for i in X_test:
#     i[0] = i[0]/X_max2[0]
#     i[1] = i[1]/X_max2[1]
#     i[2] = i[2]/X_max2[2]
#Y_test = Y_train_sat[-num_test:]
#%%
threshold2 = 0.001
zero_indx = np.where(Y<threshold2)
one_indx = np.where(Y>=threshold2)
zero_indx_test = np.where(Y_test<threshold2)
one_indx_test = np.where(Y_test>=threshold2)
# For the regressor
X_reg = X[one_indx]
Y_reg = Y[one_indx]
X_reg_test = X_test[one_indx_test]
Y_reg_test = Y_test[one_indx_test]

# For the classifier. The Y data now will become only 0s and 1s and the X data
# are the same as will the test data
Y[zero_indx] = 0
Y[one_indx] = 1
Y_test[zero_indx_test] = 0
Y_test[one_indx_test] = 1

from imblearn.over_sampling import SMOTE, ADASYN
from imblearn.under_sampling import RandomUnderSampler
underpasta = RandomUnderSampler(random_state=42)
pasta = ADASYN(random_state=42)
#X_resampled, Y_resampled = pasta.fit_resample(X, Y)
#X_resampled, Y_resampled = underpasta.fit_resample(X, Y)
X_resampled, Y_resampled = X,Y
#%%
from sklearn.model_selection import cross_val_score,cross_val_predict,cross_validate,KFold
from sklearn.metrics import confusion_matrix,precision_score, recall_score,f1_score,precision_recall_curve,roc_curve, roc_auc_score
kfold = KFold(n_splits=10, shuffle=True, random_state=42)

from sklearn.svm import SVC
svc = SVC(random_state=42, C=100,gamma=8,kernel="rbf",degree=3,class_weight="balanced")
svc.fit(X_resampled,Y_resampled)
preds_SVM = svc.predict(X_test)
#print(f"SVM score: {svc.score(X_test,Y_test)}")

# Cross validation and confusion matrix on the training set
CrossValScore_train = cross_val_score(svc, X_resampled, Y_resampled, cv=kfold, scoring="balanced_accuracy")
print(array2bmatrix(CrossValScore_train)) # use this for the report

y_train_pred_svc = cross_val_predict(svc, X_resampled, Y_resampled, cv=kfold)
conf_mat_train = confusion_matrix(Y_resampled,y_train_pred_svc)
print(array2bmatrix(conf_mat_train))

precision_score(Y_resampled, y_train_pred_svc)
recall_score(Y_resampled,y_train_pred_svc)

# Some metrics on the training set again
y_scores_svc = cross_val_predict(svc, X_resampled, Y_resampled, cv=kfold,method="decision_function")
precisions, recalls, thresholds = precision_recall_curve(Y_resampled, y_scores_svc)
# Set up separate figures and save them
plt.figure()
plot_precision_recall_vs_threshold(precisions, recalls, thresholds)
plt.savefig("figures/SVM/precision_recall_vs_threshold_train",transparent=False,bbox_inches="tight")

fpr, tpr, thresholds = roc_curve(Y_resampled,y_scores_svc)
plt.figure()
plot_roc_curve(fpr,tpr)
plt.savefig("figures/SVM/roc_curve_train",transparent=False,bbox_inches="tight")

roc_auc_score(Y_resampled,y_scores_svc)

# Using information on the above metrics we tune the decision threshold to get
# better recall
#y_train_pred_svc90 = (y_scores_svc > -.9999) # This is for kernel=poly
y_train_pred_svc90 = (y_scores_svc > -0.8) # This is for rbf
recall_score(Y_resampled, y_train_pred_svc90)
precision_score(Y_resampled, y_train_pred_svc90)
conf_mat_train_tuned = confusion_matrix(Y_resampled,y_train_pred_svc90)
#print(array2bmatrix(conf_mat_train_tuned))

# Same stuff on the test set
CrossValScore_test = cross_val_score(svc, X_test, Y_test, cv=kfold, scoring="balanced_accuracy")
y_scores_svc_test = cross_val_predict(svc, X_test, Y_test, cv=kfold,method="decision_function")
#y_train_pred_svc90 = (y_scores_svc_test > -.999)
y_test_pred_svc90 = (y_scores_svc_test > -0.8)
conf_mat_test_tuned = confusion_matrix(Y_test,y_test_pred_svc90)
print(array2bmatrix(conf_mat_test_tuned))
recall_score(Y_test,y_test_pred_svc90)
precision_score(Y_test,y_test_pred_svc90)

precisions, recalls, thresholds = precision_recall_curve(Y_test, y_scores_svc_test)
plt.figure()
plot_precision_recall_vs_threshold(precisions, recalls, thresholds)
plt.savefig("figures/SVM/precision_recall_vs_threshold_test",transparent=False,bbox_inches="tight")

fpr, tpr, thresholds = roc_curve(Y_test,y_scores_svc_test)
plt.figure()
plot_roc_curve(fpr,tpr)
plt.savefig("figures/SVM/roc_curve_test",transparent=False,bbox_inches="tight")
#y_sscores = svc.decision_function(X_test)
roc_auc_score(Y_test,y_scores_svc_test)
#%%
import seaborn as sns
# Get the predictions using the tuning
predictions = np.zeros(Y_test.shape[0])
predictions[y_test_pred_svc90] = 1
conf_mat_tuned_preds = confusion_matrix(Y_test,predictions)
conf_mat_preds = confusion_matrix(Y_test,svc.predict(X_test))
print(array2bmatrix(conf_mat_tuned_preds))
print(array2bmatrix(conf_mat_preds))
# It just overclassifies class 1 it doesnt look right
bins = 25
plt.figure()
sns.distplot(svc.predict(X_test),kde=False,bins=bins,label="Predictions",color="C0")
#sns.distplot(predictions,kde=False,bins=bins,label="Predictions",color="C0")
sns.distplot(Y_test,kde=False,bins=bins,label="True labels",color="C2")
plt.legend()

plt.savefig("figures/SVM/hist_comparison",transparent=False,bbox_inches="tight")























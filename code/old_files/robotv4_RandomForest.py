import numpy as np
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from funcs import set_rc_params,plot_precision_recall_vs_threshold,plot_roc_curve,array2bmatrix
set_rc_params()
import scipy.stats as stats
import seaborn as sns

Y_train_stations = np.load("AI_DATA/Y_train_stations.npy",allow_pickle=True)
Y_train_sat = np.load("AI_DATA/Y_train_sat.npy",allow_pickle=True)
X_train_st = np.load("AI_DATA/X_train_st.npy",allow_pickle=True)
X_train_sat = np.load("AI_DATA/X_train_sat.npy",allow_pickle=True)


#Y_train_stations = np.load("AI_DATA/Y_train_stations.npy")
#Y_train_sat = np.load("AI_DATA/Y_train_sat.npy")
#X_train_st = np.load("AI_DATA/X_train_st2.npy",allow_pickle=True)
#X_train_sat = np.load("AI_DATA/X_train_sat2.npy",allow_pickle=True)

X = np.vstack((X_train_st,X_train_sat))
Y = np.hstack((Y_train_stations,Y_train_sat))
# NORMALIZE
# This is most likely need for the neural nets but for the other classifiers
# maybe it is best to try the scaler from sklearn
#X_max = np.max(X,axis=0)
#for i in X:
#    i[1] = i[1]/X_max[1]
#    i[2] = i[2]/X_max[2]
#    i[4] = i[4]/X_max[4]
#    i[5] = i[5]/X_max[5]

# Delete the ecc,omega and mean_motion collumns
X = np.delete(X,(0),axis=1)
X = np.delete(X,(2),axis=1)
X = np.delete(X,(3),axis=1)
#
#X = np.delete(X,((0),(1)),axis=1)
#X = np.delete(X,((4),(5)),axis=1)
#X = np.delete(X,((6),(7)),axis=1)

from sklearn.model_selection import train_test_split
X, X_test, Y, Y_test = train_test_split(X, Y, test_size=0.2, random_state=1)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X = scaler.fit_transform(X)
X_test = scaler.fit_transform(X_test)


#X_max2 = np.max(X_test,axis=0)
#for i in X_test:
#    i[0] = i[0]/X_max2[0]
#    i[1] = i[1]/X_max2[1]
#    i[2] = i[2]/X_max2[2]
#Y_test = Y_train_sat[-num_test:]
#%%
threshold2 = 0.001
zero_indx = np.where(Y<threshold2)
one_indx = np.where(Y>=threshold2)
zero_indx_test = np.where(Y_test<threshold2)
one_indx_test = np.where(Y_test>=threshold2)
# For the regressor
X_reg = X[one_indx]
Y_reg = Y[one_indx]
X_reg_test = X_test[one_indx_test]
Y_reg_test = Y_test[one_indx_test]

# For the classifier. The Y data now will become only 0s and 1s and the X data
# are the same as will the test data
Y[zero_indx] = 0
Y[one_indx] = 1
Y_test[zero_indx_test] = 0
Y_test[one_indx_test] = 1

from imblearn.over_sampling import SMOTE, ADASYN
from imblearn.under_sampling import RandomUnderSampler
underpasta = RandomUnderSampler(random_state=42)
pasta = ADASYN(random_state=42)
X_resampled, Y_resampled = pasta.fit_resample(X, Y)
#X_resampled, Y_resampled = underpasta.fit_resample(X, Y)
#X_resampled, Y_resampled = X,Y
#from sklearn.preprocessing import PolynomialFeatures
#poly_feat = PolynomialFeatures(degree=3)
#X_resampled = poly_feat.fit_transform(X_resampled)
#X_test = poly_feat.fit_transform(X_test)
#plt.figure
#sns.distplot(Y,kde=False,bins=10)
##plt.legend()
#plt.savefig("figures/RandomForest/Y_train",bbox_inches="tight")
#
#plt.figure()
#sns.distplot(Y_resampled,kde=False,bins=10)
##plt.legend()
#plt.savefig("figures/RandomForest/Y_resampled",bbox_inches="tight")
#%%
#from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score,cross_val_predict,cross_validate,KFold,RandomizedSearchCV
from sklearn.metrics import confusion_matrix,precision_score, recall_score,f1_score,precision_recall_curve,roc_curve, roc_auc_score
kfold = KFold(n_splits=10, shuffle=True, random_state=42)
from sklearn.ensemble import RandomForestClassifier

## run randomized search
#forest_clf = RandomForestClassifier(random_state=42,n_estimators=100)
#
#rad = stats.randint(1,11)
#rad2 = stats.randint(2,11)
#param_dist = {"max_depth": [3,4,5,None],
#              "max_features": [1,2,3],
#              "min_samples_split": [2,3,4,5],
#              "bootstrap": [True, False],
#              "criterion": ["gini", "entropy"]}
#
#n_iter_search = 20
#random_search = RandomizedSearchCV(forest_clf,param_distributions=param_dist, n_iter=n_iter_search, cv=kfold, iid=False)
#random_search.fit(X_resampled,Y_resampled)

forest_clf = RandomForestClassifier(random_state=42,n_estimators=50,max_depth=200,min_samples_leaf=3,criterion="gini",bootstrap=True,n_jobs=-1)

forest_clf.fit(X_resampled,Y_resampled)
# Cross validation and confusion matrix on the training set
CrossValScore = cross_val_score(forest_clf, X_resampled, Y_resampled, cv=kfold, scoring="balanced_accuracy")
print(array2bmatrix(CrossValScore)) # use this for the report

y_train_pred_forest = cross_val_predict(forest_clf, X_resampled, Y_resampled, cv=kfold)
conf_mat_train = confusion_matrix(Y_resampled,y_train_pred_forest)
print(array2bmatrix(conf_mat_train))
print("Recall= ",recall_score(Y_resampled,y_train_pred_forest))

# Some metrics on the training set again
y_probas_forest = cross_val_predict(forest_clf, X_resampled, Y_resampled, cv=kfold,method="predict_proba")
y_scores_forest = y_probas_forest[:, 1]
fpr_forest, tpr_forest, thresholds_forest = roc_curve(Y_resampled,y_scores_forest)
precisions3, recalls3, thresholds3 = precision_recall_curve(Y_resampled, y_scores_forest)
# Set up separate figures and save them
plt.figure()
plot_precision_recall_vs_threshold(precisions3, recalls3, thresholds3)
plt.savefig("figures/RandomForest/precision_recall_vs_threshold_train",transparent=False,bbox_inches="tight")
plt.figure()
plot_roc_curve(fpr_forest, tpr_forest)
plt.savefig("figures/RandomForest/roc_curve_train",transparent=False,bbox_inches="tight")

roc_auc_score(Y_resampled,y_scores_forest)


# Using information on the above metrics we tune the decision threshold to get
# better recall
y_scores_forest = y_probas_forest[:, 1]
y_train_pred_forest85 = (y_scores_forest > 0.01) # keep the false stuff under 250
recall_score(Y_resampled, y_train_pred_forest85)
precision_score(Y_resampled, y_train_pred_forest85)
conf_mat_train_tuned = confusion_matrix(Y_resampled,y_train_pred_forest85)
print(array2bmatrix(conf_mat_train_tuned))
#fpr_forest, tpr_forest, thresholds_forest = roc_curve(Y_resampled,y_scores_forest)
#precisions3, recalls3, thresholds3 = precision_recall_curve(Y_resampled, y_scores_forest)
#plot_precision_recall_vs_threshold(precisions3, recalls3, thresholds3)
#plot_roc_curve(fpr_forest, tpr_forest)

# Test set
y_probas_forest_test = cross_val_predict(forest_clf, X_test, Y_test, cv=kfold,method="predict_proba")
CrossValScore_test = cross_val_score(forest_clf, X_test, Y_test, cv=kfold, scoring="balanced_accuracy")
print(array2bmatrix(CrossValScore_test))
#y_train_pred_forest85_test = cross_val_predict(forest_clf, X_test, Y_test, cv=kfold,method="predict")
y_scores_forest_test = y_probas_forest_test[:, 1] # score=probability of the positive class
y_train_pred_forest85_test = (y_scores_forest_test > 0.01) # keep the false stuff under 1k
recall_score(Y_test, y_train_pred_forest85_test)
conf_mat_test85 = confusion_matrix(Y_test,y_train_pred_forest85_test)

fpr_forest, tpr_forest, thresholds_forest = roc_curve(Y_test,y_scores_forest_test)
precisions3, recalls3, thresholds3 = precision_recall_curve(Y_test, y_scores_forest_test)

plt.figure()
plot_precision_recall_vs_threshold(precisions3, recalls3, thresholds3)
plt.savefig("figures/RandomForest/precision_recall_vs_threshold_test",transparent=False,bbox_inches="tight")

plt.figure()
plot_roc_curve(fpr_forest, tpr_forest)
plt.savefig("figures/RandomForest/roc_curve_test",transparent=False,bbox_inches="tight")

roc_auc_score(Y_test,y_scores_forest_test)
#%%
# Get the predictions using the tuning
predictions = np.zeros(Y_test.shape[0])
predictions[y_train_pred_forest85_test] = 1
conf_mat_tuned_preds = confusion_matrix(Y_test,predictions)
conf_mat_preds = confusion_matrix(Y_test,forest_clf.predict(X_test))
print(array2bmatrix(conf_mat_tuned_preds))
print(array2bmatrix(conf_mat_preds))
bins = 25
plt.figure()
#sns.distplot(predictions,kde=False,bins=bins,label="Predictions",color="C0")
sns.distplot(forest_clf.predict(X_test),kde=False,bins=bins,label="Predictions",color="C0")
sns.distplot(Y_test,kde=False,bins=bins,label="True labels",color="C2")
plt.legend()
plt.savefig("figures/RandomForest/hist_comparison",transparent=False,bbox_inches="tight")


















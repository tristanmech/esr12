# Here we do the sampling and the data processing
import numpy as np
from skyfield.api import load

# Primary objects
satellites = load.tle("CelesTrak_data/active.txt")
stations = load.tle("CelesTrak_data/stations.txt")
# Secondary objects
indian_debris = load.tle("CelesTrak_data/2019-006_indian_debris.txt")
breeze_debris = load.tle("CelesTrak_data/breeze_breakup.txt")
cosmos_2251_debris = load.tle("CelesTrak_data/cosmos-2251-debris.txt")
iridium_debris = load.tle("CelesTrak_data/iridium-33-debris.txt")

# Make each object appear only once
# WARNING: This also destroys the order so every time this is run the objects
# are in different order!
satellites = list(set(satellites.values()))
stations = list(set(stations.values()))
indian_debris = list(set(indian_debris.values()))
breeze_debris = list(set(breeze_debris.values()))
cosmos_2251_debris = list(set(cosmos_2251_debris.values()))
iridium_debris = list(set(iridium_debris.values()))
# Combined debris
debris = np.hstack((indian_debris,breeze_debris,cosmos_2251_debris,iridium_debris))
#objects = [stations,satellites,indian_debris,breeze_debris,cosmos_2251_debris,
#           iridium_debris]

# Create a list of what is what with their index for later reference
# The format is:
# obj_index[0]: The number of the object in the ordered list
# obj_index[1]: The index of the object in the position/velocity array
# obj_index[2]: The name of the object

# Stations
#num = 0
#station_index = []
#for k in range(len(stations)):
##    station_index.append(f"{num},{k},{k+3},{stations[k].name}")
#    station_index.append([[num],[k,k+3],stations[k].name])
#    num += 1

num = 0
l = 0
station_index = np.zeros((len(stations),3),dtype=object)
for k in range(len(stations)):
    station_index[k,0] = num
    station_index[k,1] = [l,l+3]
    station_index[k,2] = stations[k].name
    num += 1
    l += 3
# Satellites
num = 0
l = 0
satellite_index = np.zeros((len(satellites),3),dtype=object)
for k in range(len(satellites)):
    satellite_index[k,0] = num
    satellite_index[k,1] = [l,l+3]
    satellite_index[k,2] = satellites[k].name
    num += 1
    l += 3
# These all have the same name for some reason so let's add a
# number at the end so we can tell them apart
# Indian debris
num = 0
l = 0
indian_index = np.zeros((len(indian_debris),3),dtype=object)
for k in range(len(indian_debris)):
    indian_index[k,0] = num
    indian_index[k,1] = [l,l+3]
    indian_index[k,2] = indian_debris[k].model.satnum
    num += 1
    l += 3
# Breeze debris
num = 0
l = 0
breeze_index = np.zeros((len(breeze_debris),3),dtype=object)
for k in range(len(breeze_debris)):
    breeze_index[k,0] = num
    breeze_index[k,1] = [l,l+3]
    breeze_index[k,2] = breeze_debris[k].model.satnum
    num += 1
    l += 3
# Iridium debris
num = 0
l = 0
iridium_index = np.zeros((len(iridium_debris),3),dtype=object)
for k in range(len(iridium_debris)):
    iridium_index[k,0] = num
    iridium_index[k,1] = [l,l+3]
    iridium_index[k,2] = iridium_debris[k].model.satnum
    num += 1
    l += 3
# Cosmos debris
num = 0
l = 0
cosmos_index = np.zeros((len(cosmos_2251_debris),3),dtype=object)
for k in range(len(cosmos_2251_debris)):
    cosmos_index[k,0] = num
    cosmos_index[k,1] = [k,k+3]
    cosmos_index[k,2] = cosmos_2251_debris[k].model.satnum
    num += 1
    l += 3
# Now we have lists of all the names of the objects

ts = load.timescale()
# Set the time for the calculations
# From the day I downloaded the files to one week after
# 7 days is what they used in their paper
# Maybe we will use a bit less for computational reasons
time_start = ts.utc(2019,month=6,day=15,hour=12,minute=0,second=0.0)
time_end = ts.utc(2019,month=6,day=22,hour=12,minute=0,second=0.0)
# Array of times to do the computations
# It starts at t_start and ends one day after with computations done every
# 3 minutes
hours = np.arange(0.0,24,0.05)
time_interval = ts.utc(2019,month=6,day=15,hour=hours)

##################### POSITIONS ####################
# The position of each object is a (3,) np.array with the x,y,z coordinates
# relative to the Earth's center in the Geocentric Celestial Reference System
# (GCRS) in km
# So to hold all the positions we need an array with objects.size() rows
# and 3 collumns
# One array will hold the primary objects' positions and another the secondary
# Also we need one array for each time (start,end)


# PRIMARY OBJECTS

# The positions will be stacked next to each other so we have another satellite
# every 3 collumns. The array is initialized with a collumn of zeros to start
# the hstack sequence, it will be removed later

# Stations
station_positions = np.zeros((len(hours),1))
for i,station_object in enumerate(stations):
    current_station_positions = station_object.at(time_interval).position.km.T # (480,3) array, all the positions of the current station in the loop
    station_positions = np.hstack((station_positions,current_station_positions))
# Now remove the 0s
station_positions = np.delete(station_positions,np.s_[0],axis=1)
# Now we have the x,y,z position for each station, horizontally stacked

# Satellites
sat_positions = np.zeros((len(hours),1))
for i,sat_object in enumerate(satellites):
    current_sat_positions = sat_object.at(time_interval).position.km.T
    sat_positions = np.hstack((sat_positions,current_sat_positions))
# Now remove the 0s
sat_positions = np.delete(sat_positions,np.s_[0],axis=1)

# SECONDARY OBJECTS
# Indian debris
indian_positions = np.zeros((len(hours),1))
for i,indian_object in enumerate(indian_debris):
    current_indian_positions = indian_object.at(time_interval).position.km.T
    indian_positions = np.hstack((indian_positions,current_indian_positions))
# Now remove the 0s
indian_positions = np.delete(indian_positions,np.s_[0],axis=1)

# Cosmos-2241 debris
cosmos_positions = np.zeros((len(hours),1))
for i,cosmos_object in enumerate(cosmos_2251_debris):
    current_cosmos_positions = cosmos_object.at(time_interval).position.km.T
    cosmos_positions = np.hstack((cosmos_positions,current_cosmos_positions))
# Now remove the 0s
cosmos_positions = np.delete(cosmos_positions,np.s_[0],axis=1)

# Breeze debris
breeze_positions = np.zeros((len(hours),1))
for i,breeze_object in enumerate(breeze_debris):
    current_breeze_positions = breeze_object.at(time_interval).position.km.T
    breeze_positions = np.hstack((breeze_positions,current_breeze_positions))
# Now remove the 0s
breeze_positions = np.delete(breeze_positions,np.s_[0],axis=1)

# Iridium debris
iridium_positions = np.zeros((len(hours),1))
for i,iridium_object in enumerate(iridium_debris):
    current_iridium_positions = iridium_object.at(time_interval).position.km.T
    iridium_positions = np.hstack((iridium_positions,current_iridium_positions))
# Now remove the 0s
iridium_positions = np.delete(iridium_positions,np.s_[0],axis=1)

# Assemble the debris in one big array to cross-reference them all at the same
# time later
debris_positions = np.hstack((indian_positions,breeze_positions,iridium_positions,
                              cosmos_positions))

# indian: debris_positions[:,0] - debris_positions[:,152]
# breeze: debris_positions[:,153] - debris_positions[:,188]
# iridium: debris_positions[:,189] - debris_positions[:,1100]
# cosmos: debris_positions[:,1101] - debris_positions[:,4160]

# Create indexes for the debris
num = 0
l = 0
debris_index = np.zeros((int(np.shape(debris_positions)[1]/3),3),dtype=object)
for k in range(int(np.shape(debris_positions)[1]/3)):
    debris_index[k,0] = num
    debris_index[k,1] = [l,l+3]
#    debris_index[k,2] = stations[k].name
    num += 1
    l += 3
for k in range(int(np.shape(debris_positions)[1]/3)):
#    print(k)
    if k < 51:
        debris_index[k,2] = indian_debris[k].model.satnum
    elif k >= 51 and k < 63:
        debris_index[k,2] = breeze_debris[k - (51)].model.satnum
    elif k >= 63 and k < 367:
        debris_index[k,2] = iridium_debris[k - (63)].model.satnum
    elif k >= 367:
        debris_index[k,2] = cosmos_2251_debris[k - (367)].model.satnum



##################### VELOCITIES ####################
# The structure is the same as with the positions
# They are stored in km/s
# Stations
station_velocities = np.zeros((len(hours),1))
for i,station_object in enumerate(stations):
    current_station_velocities = station_object.at(time_interval).velocity.km_per_s.T # (480,3) array, all the velocities of the current station in the loop
    station_velocities = np.hstack((station_velocities,current_station_velocities))
# Now remove the 0s
station_velocities = np.delete(station_velocities,np.s_[0],axis=1)
# Now we have the x,y,z velocity for each station, horizontally stacked

# Satellites
sat_velocities = np.zeros((len(hours),1))
for i,sat_object in enumerate(satellites):
    current_sat_velocities = sat_object.at(time_interval).velocity.km_per_s.T
    sat_velocities = np.hstack((sat_velocities,current_sat_velocities))
# Now remove the 0s
sat_velocities = np.delete(sat_velocities,np.s_[0],axis=1)

# SECONDARY OBJECTS
# Indian debris
indian_velocities = np.zeros((len(hours),1))
for i,indian_object in enumerate(indian_debris):
    current_indian_velocities = indian_object.at(time_interval).velocity.km_per_s.T
    indian_velocities = np.hstack((indian_velocities,current_indian_velocities))
# Now remove the 0s
indian_velocities = np.delete(indian_velocities,np.s_[0],axis=1)

# Cosmos-2241 debris
cosmos_velocities = np.zeros((len(hours),1))
for i,cosmos_object in enumerate(cosmos_2251_debris):
    current_cosmos_velocities = cosmos_object.at(time_interval).velocity.km_per_s.T
    cosmos_velocities = np.hstack((cosmos_velocities,current_cosmos_velocities))
# Now remove the 0s
cosmos_velocities = np.delete(cosmos_velocities,np.s_[0],axis=1)

# Breeze debris
breeze_velocities = np.zeros((len(hours),1))
for i,breeze_object in enumerate(breeze_debris):
    current_breeze_velocities = breeze_object.at(time_interval).velocity.km_per_s.T
    breeze_velocities = np.hstack((breeze_velocities,current_breeze_velocities))
# Now remove the 0s
breeze_velocities = np.delete(breeze_velocities,np.s_[0],axis=1)

# Iridium debris
iridium_velocities = np.zeros((len(hours),1))
for i,iridium_object in enumerate(iridium_debris):
    current_iridium_velocities = iridium_object.at(time_interval).velocity.km_per_s.T
    iridium_velocities = np.hstack((iridium_velocities,current_iridium_velocities))
# Now remove the 0s
iridium_velocities = np.delete(iridium_velocities,np.s_[0],axis=1)

debris_velocities = np.hstack((indian_velocities,breeze_velocities,iridium_velocities,
                              cosmos_velocities))

#%%
##################### SAVE FOR LATER USE ####################
# Primary
np.save("data/stations",stations)
np.save("positions/station_positions",station_positions)
np.save("velocities/station_velocities",station_velocities)
np.save("positions/station_index",station_index)

np.save("data/satellites",satellites)
np.save("positions/sat_positions",sat_positions)
np.save("velocities/sat_velocities",sat_velocities)
np.save("positions/satellite_index",satellite_index)

# Secondary
np.save("data/indian_debris",indian_debris)
np.save("positions/indian_positions",indian_positions)
np.save("velocities/indian_velocities",indian_velocities)
np.save("positions/indian_index",indian_index)

np.save("data/breeze_debris",breeze_debris)
np.save("positions/breeze_positions",breeze_positions)
np.save("velocities/breeze_velocities",breeze_velocities)
np.save("positions/breeze_index",breeze_index)

np.save("data/iridium_debris",iridium_debris)
np.save("positions/iridium_positions",iridium_positions)
np.save("velocities/iridium_velocities",iridium_velocities)
np.save("positions/iridium_index",iridium_index)

np.save("data/cosmos_2251_debris",cosmos_2251_debris)
np.save("positions/cosmos_positions",cosmos_positions)
np.save("velocities/cosmos_velocities",cosmos_velocities)
np.save("positions/cosmos_index",cosmos_index)

np.save("data/debris",debris)
np.save("positions/debris_positions",debris_positions)
np.save("velocities/debris_velocities",debris_velocities)
np.save("positions/debris_index",debris_index)

#%%
##################### ORBITAL ELEMENTS ####################
# Here we will extract the orbital elements of each object to train the network
#from skyfield.elementslib import osculating_elements_of

# We load them here to get them in array form
stations = np.load("data/stations.npy",allow_pickle=True)
satellites = np.load("data/satellites.npy",allow_pickle=True)
debris = np.load("CelesTrak_data/debris.npy",allow_pickle=True)
#t = ts.utc(2019,month=6,day=15,hour=0)

#pos = stations[0].at(time_interval)
#elements = osculating_elements_of(pos)
#i = elements.inclination.degrees
#Omega = elements.longitude_of_ascending_node.degrees # capital omega
#e = elements.eccentricity
#omega = elements.argument_of_periapsis.degrees
#mean_anomaly = elements.mean_anomaly.degrees
#theta = elements.true_anomaly.degrees
#mean_motion = elements.mean_motion_per_day.degrees
#a = elements.semi_major_axis.km

# STATIONS
ecc_stations = np.zeros((stations.shape[0],1))
inc_stations = np.zeros((stations.shape[0],1))
OMEGA_stations = np.zeros((stations.shape[0],1))
omega_stations = np.zeros((stations.shape[0],1))
mean_anomaly_stations = np.zeros((stations.shape[0],1))
mean_motion_stations = np.zeros((stations.shape[0],1))
station_names = np.zeros((stations.shape[0],1),dtype=object)

for i,obj in enumerate(stations):
    ecc_stations[i] = obj.model.ecco
    inc_stations[i] = np.math.degrees(obj.model.inclo)
    OMEGA_stations[i] = np.math.degrees(obj.model.nodeo)
    omega_stations[i] = np.math.degrees(obj.model.argpo)
    mean_anomaly_stations[i] = np.math.degrees(obj.model.mo)
    mean_motion_stations[i] = np.math.degrees(obj.model.no)
    station_names[i] = obj.name

# Stack them together and now each line is the orbital elements vector of an
# object. Add the name/satnum at the end for easier referece
OE_stations = np.hstack((ecc_stations,inc_stations,OMEGA_stations,
                         omega_stations,mean_anomaly_stations,
                         mean_motion_stations,station_names))


# SATELLITES
ecc_satellites = np.zeros((satellites.shape[0],1))
inc_satellites = np.zeros((satellites.shape[0],1))
OMEGA_satellites = np.zeros((satellites.shape[0],1))
omega_satellites = np.zeros((satellites.shape[0],1))
mean_anomaly_satellites = np.zeros((satellites.shape[0],1))
mean_motion_satellites = np.zeros((satellites.shape[0],1))
satellite_names = np.zeros((satellites.shape[0],1),dtype=object)

for i,obj in enumerate(satellites):
    ecc_satellites[i] = obj.model.ecco
    inc_satellites[i] = np.math.degrees(obj.model.inclo)
    OMEGA_satellites[i] = np.math.degrees(obj.model.nodeo)
    omega_satellites[i] = np.math.degrees(obj.model.argpo)
    mean_anomaly_satellites[i] = np.math.degrees(obj.model.mo)
    mean_motion_satellites[i] = np.math.degrees(obj.model.no)
    satellite_names[i] = obj.name

OE_satellites = np.hstack((ecc_satellites,inc_satellites,OMEGA_satellites,
                           omega_satellites,mean_anomaly_satellites,
                           mean_motion_satellites,satellite_names))

# DEBRIS
ecc_debris = np.zeros((debris.shape[0],1))
inc_debris = np.zeros((debris.shape[0],1))
OMEGA_debris = np.zeros((debris.shape[0],1))
omega_debris = np.zeros((debris.shape[0],1))
mean_anomaly_debris = np.zeros((debris.shape[0],1))
mean_motion_debris = np.zeros((debris.shape[0],1))
debris_satnums = np.zeros((debris.shape[0],1),dtype=object)

for i,obj in enumerate(debris):
    ecc_debris[i] = obj.model.ecco
    inc_debris[i] = np.math.degrees(obj.model.inclo)
    OMEGA_debris[i] = np.math.degrees(obj.model.nodeo)
    omega_debris[i] = np.math.degrees(obj.model.argpo)
    mean_anomaly_debris[i] = np.math.degrees(obj.model.mo)
    mean_motion_debris[i] = np.math.degrees(obj.model.no)
    debris_satnums[i] = obj.model.satnum

OE_debris = np.hstack((ecc_debris,inc_debris,OMEGA_debris,
                           omega_debris,mean_anomaly_debris,
                           mean_motion_debris,debris_satnums))

np.save("data/OE_stations",OE_stations)
np.save("data/OE_satellites",OE_satellites)
np.save("data/OE_debris",OE_debris)
#%%
# Lets fiddle with the data from the satellites
# Perturb each value of the OE with a random number drawn from a normal with
# loc = value and stdev, lets say 0.3 times the loc

# ecc_norm = np.random.normal(loc=0,scale=0.3*np.max(ecc_satellites))
# inc_norm = np.random.normal(loc=0,scale=0.3*np.max(inc_satellites))
# OMEGA_norm = np.random.normal(loc=0,scale=0.3*np.max(OMEGA_satellites))
# omega_norm = np.random.normal(loc=0,scale=0.3*np.max(omega_satellites))
# mean_anomaly_norm = np.random.normal(loc=0,scale=0.3*np.max(mean_anomaly_satellites))
# mean_motion_norm = np.random.normal(loc=0,scale=0.3*np.max(mean_motion_satellites))
OE_satellites_new = np.hstack((ecc_satellites,inc_satellites,OMEGA_satellites,
                           omega_satellites,mean_anomaly_satellites,
                           mean_motion_satellites))

sat_fiddle = lambda a:np.random.normal(loc=a,scale=0.3*a)

for i,oe in enumerate(OE_satellites_new.T):
    OE_satellites_new.T[i] = sat_fiddle(oe)

# Add the names now









































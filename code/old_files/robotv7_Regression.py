import numpy as np
import matplotlib.pyplot as plt
from funcs import set_rc_params,plot_precision_recall_vs_threshold,plot_roc_curve
set_rc_params()
import seaborn as sns

Y_train_stations = np.load("AI_DATA/Y_train_stations.npy",allow_pickle=True)
Y_train_sat = np.load("AI_DATA/Y_train_sat.npy",allow_pickle=True)
X_train_st = np.load("AI_DATA/X_train_st2.npy",allow_pickle=True)
X_train_sat = np.load("AI_DATA/X_train_sat2.npy",allow_pickle=True)

X = np.vstack((X_train_st,X_train_sat))
Y = np.hstack((Y_train_stations,Y_train_sat))

#X = X**2

X_max = np.max(X,axis=0)
for i in X:
    i[1] = i[1]/X_max[1]
    i[2] = i[2]/X_max[2]
    i[4] = i[4]/X_max[4]

# Delete the ecc,omega and mean_motion collumns
#X = np.delete(X,(0),axis=1)
#X = np.delete(X,(2),axis=1)
#X = np.delete(X,(3),axis=1)


X = np.delete(X,((0),(1)),axis=1)
X = np.delete(X,((4),(5)),axis=1)
X = np.delete(X,((6),(7)),axis=1)
from sklearn.model_selection import train_test_split
X, X_test, Y, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

#from sklearn.preprocessing import StandardScaler
#scaler = StandardScaler()
#X = scaler.fit_transform(X)
#X_test = scaler.fit_transform(X_test)
#%%
# Robot 5 will be a multiclass version so we will define some thresholds and
# assign each probability to a different class
# Since the dataset is already massively imbalanced maybe try this with the _reg
# sets
threshold2 = 0.001
zero_indx = np.where(Y<threshold2)
one_indx = np.where(Y>=threshold2)
zero_indx_test = np.where(Y_test<threshold2)
one_indx_test = np.where(Y_test>=threshold2)
# For the multiclass
X_reg = X[one_indx]
Y_reg = Y[one_indx]
X_reg_test = X_test[one_indx_test]
Y_reg_test = Y_test[one_indx_test]
# For the classifier. The Y data now will become only 0s and 1s and the X data
# are the same as will the test data
Y[zero_indx] = 0
Y[one_indx] = 1
Y_test[zero_indx_test] = 0
Y_test[one_indx_test] = 1

#from imblearn.over_sampling import SMOTE, ADASYN
#from imblearn.under_sampling import RandomUnderSampler
#underpasta = RandomUnderSampler(random_state=42)
#pasta = SMOTE(random_state=42)
#X_resampled, Y_resampled = pasta.fit_resample(X_reg, Y_reg)
#%%
def GetMollifiedFunction(f,mollifier,N,cmode='same'):
    eps = np.finfo(float).eps
    if  mollifier == 'standard':
        x  = np.linspace(-1+eps,1-eps,N)
        y  = np.exp(1./(x**2-1))
        y /= sum(y)
    elif mollifier == 'box':
        y  = np.ones(N)/N
    
    return np.convolve(f,y,cmode)
def SmoothAeroCoefficients(a,cl,cpNum):
    # Regularization of the viscous database (helps with adjoint computations)
    degree  = 3
    ds      = 1e-2
    # Fit B-spline to viscous database
    clCurve = fitting.approximate_curve(zip(a,cl),degree,ctrlpts_size=cpNum)
    clCurve.delta = ds
    # Evaluate the B-spline
    clSmoothed   = clCurve.evalpts
    aSmoo,clSmoo = zip(*clSmoothed)
    return [aSmoo,clSmoo]

from geomdl import BSpline,fitting

#Y_reg = SmoothAeroCoefficients(X_reg,Y_reg,60)
Y_reg = GetMollifiedFunction(Y_reg,"standard",10,cmode="same")
Y_reg_test = GetMollifiedFunction(Y_reg_test,"standard",10,cmode="same")
#%%
from networks import regression_model,build_model
from keras.callbacks import EarlyStopping
from keras.wrappers.scikit_learn import KerasRegressor
early_stopping = EarlyStopping(monitor='val_mean_squared_error', patience=5)

from sklearn.model_selection import cross_val_score,cross_val_predict,cross_validate,KFold
from sklearn.metrics import confusion_matrix,precision_score, recall_score,f1_score,precision_recall_curve,roc_curve, roc_auc_score
kfold = KFold(n_splits=10, shuffle=True, random_state=42)

reg = regression_model()
hist = reg.fit(X_reg,Y_reg,epochs=150,batch_size=10,validation_split=0.3,        verbose=0,callbacks=[early_stopping])
preds = reg.predict(X_reg)

sns.distplot(preds,kde=False,bins=15,label="Predictions")
sns.distplot(Y_reg,kde=False,bins=15,color="C2",label="True targets")
plt.legend()
plt.savefig("figures/regression/keras_hist",bbox_inches='tight')

plt.figure()
plt.plot(hist.history["loss"])
plt.legend()

preds = reg.predict(X_reg_test)

plt.figure()
#plt.plot(preds,label='Keras')
#plt.plot(Y_reg_test,label="True targets")
plt.scatter(preds,Y_reg_test)
plt.legend()
plt.savefig("figures/regression/keras",bbox_inches='tight')
#%%
from sklearn.ensemble import RandomForestRegressor
forest_clf = RandomForestRegressor(random_state=42,n_estimators=40,max_depth=20,criterion="mse")

forest_clf.fit(X_reg,Y_reg)
preds = forest_clf.predict(X_reg_test)
bins=15
plt.figure()
#sns.distplot(preds,label='RandomForest',kde=False,bins=bins)
#sns.distplot(Y_reg_test,label="True targets",kde=False,color="C2",bins=bins)
plt.scatter(forest_clf.predict(X_reg),Y_reg)
plt.legend()

plt.figure()
plt.plot(forest_clf.predict(X_reg),label="Random Forest")
plt.plot(Y_reg,label="True targets")
plt.legend()

print(forest_clf.score(X_reg_test,Y_reg_test))
#%%
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import VotingRegressor
from sklearn.svm import SVR

reg1 = GradientBoostingRegressor(random_state=42, n_estimators=50,criterion="mse")
reg2 = RandomForestRegressor(random_state=42, n_estimators=50,criterion="mse")
svr = SVR(C=5,gamma=10,kernel="rbf",degree=5)
ereg = VotingRegressor([("sgd",reg1),('rf', reg2),("svr",svr)])


reg1.fit(X_reg,Y_reg)
reg2.fit(X_reg,Y_reg)
svr.fit(X_reg,Y_reg)
ereg.fit(X_reg,Y_reg)

plt.figure()
sns.distplot(reg1.predict(X_reg_test),label='GradientBoosting',kde=False,bins=bins)
sns.distplot(Y_reg_test,label="True targets",color="C2",kde=False,bins=bins)
plt.legend()
plt.savefig("figures/regression/gradientboosting_hist",bbox_inches='tight')

plt.figure()
sns.distplot(reg2.predict(X_reg_test),label='RandomForest',kde=False,bins=bins)
sns.distplot(Y_reg_test,label="True targets",color="C2",kde=False,bins=bins)
plt.legend()
plt.savefig("figures/regression/randomforest_hist",bbox_inches='tight')

plt.figure()
sns.distplot(svr.predict(X_reg_test),label='SVR',kde=False,bins=bins)
sns.distplot(Y_reg_test,label="True targets",color="C2",kde=False,bins=bins)
plt.legend()
plt.savefig("figures/regression/svr_hist",bbox_inches='tight')

plt.figure()
#sns.distplot(reg1.predict(X_reg_test),label='GradientBoosting',kde=False,bins=bins)
#sns.distplot(reg2.predict(X_reg_test),label='RandomForest',kde=False,bins=bins)
#sns.distplot(svr.predict(X_reg_test),label='SVR',kde=False,bins=bins)
sns.distplot(ereg.predict(X_reg_test),label='Voting',kde=False,bins=bins)
sns.distplot(Y_reg_test,label="True targets",color="C2",kde=False,bins=bins)
plt.legend()
plt.savefig("figures/regression/voting_hist",bbox_inches='tight')

plt.figure()
plt.plot(reg1.predict(X_reg_test),label='GradientBoosting')
plt.plot(Y_reg_test,label="True targets")
plt.legend()
plt.savefig("figures/regression/gradientboosting",bbox_inches='tight')

plt.figure()
plt.plot(forest_clf.predict(X_reg_test),label="Random Forest")
plt.plot(Y_reg_test,label="True targets")
plt.legend()
plt.savefig("figures/regression/randomforest",bbox_inches='tight')

plt.figure()
plt.plot(svr.predict(X_reg_test),label="SVR")
plt.plot(Y_reg_test,label="True targets")
plt.legend()
plt.savefig("figures/regression/svr",bbox_inches='tight')

plt.figure()
#plt.plot(reg1.predict(X_reg_test),label='GradientBoosting')
#plt.plot(reg2.predict(X_reg_test),label='RandomForest')
#plt.plot(svr.predict(X_reg_test),label="SVR")
plt.plot(ereg.predict(X_reg_test),label="Voting")
plt.plot(Y_reg_test,label="True targets")
plt.legend()
plt.savefig("figures/regression/voting",bbox_inches='tight')


#%%
from sklearn.neural_network import MLPRegressor

nn = MLPRegressor(random_state=42,activation="relu",hidden_layer_sizes=(7000,),validation_fraction=0.1,solver="adam")

nn.fit(X_reg,Y_reg)
plt.figure()
#sns.distplot(nn.predict(X_reg),label='NN',kde=False,color="C0",bins=bins)
#sns.distplot(Y_reg,label="True targets",kde=False,color="C2",bins=bins)
plt.scatter(nn.predict(X_reg_test),Y_reg_test)
plt.legend()
plt.savefig("figures/regression/mlp_hist",bbox_inches='tight')

preds = nn.predict(X_reg_test)

plt.figure()
plt.plot(nn.predict(X_reg_test),label="Neural Network")
plt.plot(Y_reg_test,label="True targets")
plt.legend()
plt.savefig("figures/regression/mlp",bbox_inches='tight')



#%%
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import DotProduct, WhiteKernel
kernel = DotProduct() + WhiteKernel()
gpr = GaussianProcessRegressor(kernel=kernel,random_state=42).fit(X_reg, Y_reg)




plt.figure()
plt.plot(gpr.predict(X_reg_test),label="Neural Network")
plt.plot(Y_reg_test,label="True targets")
plt.legend()
































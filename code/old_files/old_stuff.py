# POSITIONS OF OBJECTS AT THE TIME OF ENCOUNTER
ts = load.timescale()
#encounter_time = ts.utc(2019,month=6,day=15,hour=hours)
# Primary objects
stations = np.load("data/stations.npy",allow_pickle=True)
satellites = np.load("data/satellites.npy",allow_pickle=True)
# Secondary objects
indian_debris = np.load("data/indian_debris.npy",allow_pickle=True)
breeze_debris = np.load("data/breeze_debris.npy",allow_pickle=True)
iridium_debris = np.load("data/iridium_debris.npy",allow_pickle=True)
cosmos_2251_debris = np.load("data/cosmos_2251_debris.npy",allow_pickle=True)

# PRIMARY OBJECTS
# Stations
encounter_positions_stations = []
for i,j in enumerate(stations):
    for k in range(encounters_stations.shape[0]):
        if j.name == encounters_stations[k,0]:
            encounter_positions_stations.append(j.at(ts.utc(2019,month=6,day=15,hour=encounters_stations[k][2])).position.km.T)

encounter_positions_stations = np.asarray(encounter_positions_stations)
# Satellites  
encounter_positions_satellites = []
for i,j in enumerate(satellites):
    for k in range(encounters_satellites.shape[0]):
        if j.name == encounters_satellites[k,0]:
            encounter_positions_satellites.append(j.at(ts.utc(2019,month=6,day=15,hour=encounters_satellites[k][2])).position.km.T)

encounter_positions_satellites = np.asarray(encounter_positions_satellites)
# SECONDARY OBJECTS
# The positions of the encountered debris at the time of the encounter
encounter_positions_stations_debris = []
for i,j in enumerate(debris):
    for k in range(encounters_stations.shape[0]):
        if j.model.satnum == encounters_stations[k,1]:
            encounter_positions_stations_debris.append(j.at(ts.utc(2019,month=6,day=15,hour=encounters_stations[k][2])).position.km.T)

encounter_positions_stations_debris = np.asarray(encounter_positions_stations_debris)
#
encounter_positions_satellites_debris = []
for i,j in enumerate(debris):
    for k in range(encounters_satellites.shape[0]):
        if j.model.satnum == encounters_satellites[k,1]:
            encounter_positions_satellites_debris.append(j.at(ts.utc(2019,month=6,day=15,hour=encounters_satellites[k][2])).position.km.T)

encounter_positions_satellites_debris = np.asarray(encounter_positions_satellites_debris)
# DISTANCES
#%%
vels = satellite_relative_velocities
vps = lambda vx,vy,vz: np.sqrt(vx**2 + vy**2 + vz**2)

def angles(vps,vx,vy,vz):
    theta_x = np.rad2deg(np.arccos(vx/vps))
    theta_y = np.rad2deg(np.arccos(vy/vps))
    theta_z = np.rad2deg(np.arccos(vz/vps))
    return np.array([theta_x,theta_y,theta_z]).T
sat_vps = []
for i in vels:
    sat_vps.append(vps(i[0],i[1],i[2]))
sat_vps = np.asarray(sat_vps)

sat_angles = angles(sat_vps,vels[:,0],vels[:,1],vels[:,2])
#%%
import numpy as np
##################### FAKE DATA ####################
from funcs import load_fake_data
station_positions,station_velocities,station_index,stations,sat_positions,sat_velocities,satellite_index,satellites,debris_positions,debris_velocities,debris_index,debris,sat_positions_new,sat_velocities_new,satellite_index_new,satellites_new = load_fake_data()

from funcs import encounter_detection
# Their paper had 10m = 0.01 km
miss_dist = 50 #km

time_indexes_satellites,primary_index_satellites,secondary_index_satellites,distances_satellites = encounter_detection(miss_dist,sat_positions,sat_positions_new)

print(f"\n{len(primary_index_satellites)} satellite-sat_debris encounters with less than {miss_dist}km distance detected")

from funcs import get_names
names_satellites = get_names(primary_index_satellites,satellite_index)
names_satellites_debris = get_names(secondary_index_satellites,satellite_index_new)

from skyfield.api import load
ts = load.timescale()
hours = np.arange(0.0,24,0.05)
times = ts.utc(2019,month=6,day=15,hour=hours)
times_of_encounter_satellites = np.asarray(time_indexes_satellites)
times_of_encounter_satellites = np.reshape(times_of_encounter_satellites,(times_of_encounter_satellites.shape[0]))

distances_satellites = np.asarray(distances_satellites)
dists_satellites = []
for i,j in enumerate(time_indexes_satellites):
    dists_satellites.append(distances_satellites[i,j])
dists_satellites = np.asarray(dists_satellites)
dists_satellites = np.reshape(dists_satellites,(dists_satellites.shape[0]))

encounters_satellites = np.array([names_satellites,names_satellites_debris,times_of_encounter_satellites,dists_satellites]).T
from funcs import more_than_one_encounters_mitigation
encounters_satellites = more_than_one_encounters_mitigation(encounters_satellites)

from funcs import load_FAKE_velocities,load_FAKE_positions,load_FAKE_indexes
# Velocities - km/s
FAKE_station_velocities,FAKE_sat_velocities,FAKE_debris_velocities,FAKE_sat_velocities_new = load_FAKE_velocities()
# Positions - km
FAKE_station_positions,FAKE_sat_positions,FAKE_debris_positions,FAKE_sat_positions_new = load_FAKE_positions()

# For both positions and velocities the arrays are (timesteps,xyz)
# Use the indexes to find the correct xyz value for each object at
# each time step
FAKE_station_index,FAKE_satellite_index,FAKE_debris_index,FAKE_satellite_index_new = load_FAKE_indexes()


from funcs import vel_and_pos_at_encounter
# Satellites with themselves
sat_encounter_positions,sat_encounter_velocities,sat_debris_encounter_positions,sat_debris_encounter_velocities = vel_and_pos_at_encounter(encounters_satellites,FAKE_satellite_index,FAKE_satellite_index_new,FAKE_sat_positions,FAKE_sat_velocities,FAKE_sat_positions_new,FAKE_sat_velocities_new)

# Relative velocities
# v_rel = v_sec - v_prim
rel_vel_satellites =  sat_debris_encounter_velocities - sat_encounter_velocities
# Relative positions
# p_rel = p_sec - p_prim
rel_pos_satellites = sat_debris_encounter_positions - sat_encounter_positions
from funcs import transformation_matrix,basis_vectors
# Basis vectors for each object
# Satellites
ex_satellites,ey_satellites,ez_satellites = basis_vectors(rel_vel_satellites,rel_pos_satellites)

# Transformations
# Satellites
new_rel_vel_satellites = np.zeros(3)
new_rel_pos_satellites = np.zeros(3)
for vel_vec,pos_vec,ex,ey,ez in zip(rel_vel_satellites,rel_pos_satellites,ex_satellites,ey_satellites,ez_satellites):
    # new_rel_vel_satellites.append(transformation_matrix(ex,ey,ez,vel_vec))
    # new_rel_pos_satellites.append(transformation_matrix(ex,ey,ez,pos_vec))
    new_rel_vel_satellites = np.vstack((new_rel_vel_satellites,transformation_matrix(ex,ey,ez,vel_vec)))
    new_rel_pos_satellites = np.vstack((new_rel_pos_satellites,transformation_matrix(ex,ey,ez,pos_vec)))
# remove the padding
new_rel_vel_satellites= np.delete(new_rel_vel_satellites,np.s_[0],axis=0)
new_rel_pos_satellites= np.delete(new_rel_pos_satellites,np.s_[0],axis=0)


from funcs import probability_of_collision
# Actual probability calculations!
# Algorithm from supplied paper
# Inputs: sigma_x,sigma_y,xm,ym,combined object radius R,number of terms N
theta = 0 # degrees of rotation to new coordinates. It is alway negative so I
# leave it like that and put the minus at ym like they do in the paper
r_p = 9 # in km
r_s = 1
N = 20
sigma_x = 15 # km
sigma_y = 15
R = r_p + r_s

PC_sat = probability_of_collision(N,R,sigma_x,sigma_y,theta,new_rel_pos_satellites)


from funcs import extract_OE
FAKE_satellites = np.load("FAKE_DATA/FAKE_satellites.npy",allow_pickle=True)
FAKE_satellites_new = np.load("FAKE_DATA/FAKE_satellites_new.npy",allow_pickle=True)
OE_sat_pr,OE_sat_sec = extract_OE(encounters_satellites,FAKE_satellites,FAKE_satellites_new)
OE_sat_pr = OE_sat_pr.astype(float)
OE_sat_sec = OE_sat_sec.astype(float)

ecc_train_sat = np.abs(OE_sat_pr.T[0] - OE_sat_sec.T[0])
inc_train_sat = np.abs(OE_sat_pr.T[1] - OE_sat_sec.T[1])
OMEGA_train_sat = np.abs(OE_sat_pr.T[2] - OE_sat_sec.T[2])
omega_train_sat = np.abs(OE_sat_pr.T[3] - OE_sat_sec.T[3])
mean_anomaly_train_sat = np.abs(OE_sat_pr.T[4] - OE_sat_sec.T[4])
mean_motion_train_sat = np.abs(OE_sat_pr.T[5] - OE_sat_sec.T[5])

X_train_sat = np.column_stack((ecc_train_sat.T,inc_train_sat.T,OMEGA_train_sat.T,omega_train_sat.T,mean_anomaly_train_sat.T,mean_motion_train_sat.T))

Y_train_sat = np.asarray(PC_sat)
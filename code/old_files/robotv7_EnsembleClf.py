import numpy as np
import matplotlib.pyplot as plt
from funcs import set_rc_params,plot_precision_recall_vs_threshold,plot_roc_curve,array2bmatrix
set_rc_params()
import scipy.stats as stats

Y_train_stations = np.load("AI_DATA/Y_train_stations.npy",allow_pickle=True)
Y_train_sat = np.load("AI_DATA/Y_train_sat.npy",allow_pickle=True)
X_train_st = np.load("AI_DATA/X_train_st.npy",allow_pickle=True)
X_train_sat = np.load("AI_DATA/X_train_sat.npy",allow_pickle=True)

X = np.vstack((X_train_st,X_train_sat))
Y = np.hstack((Y_train_stations,Y_train_sat))

X = np.delete(X,(0),axis=1)
X = np.delete(X,(2),axis=1)
X = np.delete(X,(3),axis=1)

from sklearn.model_selection import train_test_split
X, X_test, Y, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X = scaler.fit_transform(X)
X_test = scaler.fit_transform(X_test)
#%%
threshold2 = 0.001
zero_indx = np.where(Y<threshold2)
one_indx = np.where(Y>=threshold2)
zero_indx_test = np.where(Y_test<threshold2)
one_indx_test = np.where(Y_test>=threshold2)
# For the regressor
X_reg = X[one_indx]
Y_reg = Y[one_indx]
X_reg_test = X_test[one_indx_test]
Y_reg_test = Y_test[one_indx_test]

# For the classifier. The Y data now will become only 0s and 1s and the X data
# are the same as will the test data
Y[zero_indx] = 0
Y[one_indx] = 1
Y_test[zero_indx_test] = 0
Y_test[one_indx_test] = 1

from imblearn.over_sampling import SMOTE, ADASYN
from imblearn.under_sampling import RandomUnderSampler
underpasta = RandomUnderSampler(random_state=42)
pasta = SMOTE(random_state=42)
X_resampled, Y_resampled = pasta.fit_resample(X, Y)
#X_resampled, Y_resampled = underpasta.fit_resample(X, Y)
#%%
from sklearn.model_selection import cross_val_score,cross_val_predict,cross_validate,KFold
from sklearn.metrics import confusion_matrix,precision_score, recall_score,f1_score,precision_recall_curve,roc_curve, roc_auc_score
kfold = KFold(n_splits=10, shuffle=True, random_state=42)
import seaborn as sns
# import the models
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import VotingClassifier
# Define them
svc = SVC(random_state=42, C=100,gamma=0.83,kernel="rbf",degree=6,class_weight="balanced",probability=True)
forest_clf = RandomForestClassifier(random_state=42,n_estimators=10,max_features=3,max_depth=None,criterion="gini",bootstrap=True,class_weight="balanced")
vote_clf = VotingClassifier([("svc",svc),('rf', forest_clf)],voting="soft")

# Fit them
svc.fit(X_resampled,Y_resampled)
forest_clf.fit(X_resampled,Y_resampled)
vote_clf.fit(X_resampled,Y_resampled)
#%%
cross_val_score(vote_clf,X_resampled,Y_resampled,cv=kfold,scoring="roc_auc")
y_train_pred = cross_val_predict(svc, X_resampled, Y_resampled, cv=kfold)
confusion_matrix(Y_resampled,y_train_pred)
precision_score(Y_resampled, y_train_pred)
recall_score(Y_resampled,y_train_pred)

### Plot some metrics
y_probs_vote_clf = cross_val_predict(vote_clf, X_resampled, Y_resampled, cv=kfold,method="predict_proba")
y_scores_vote_clf= y_probs_vote_clf[:, 1]
precisions, recalls, thresholds = precision_recall_curve(Y_resampled, y_scores_vote_clf)
plot_precision_recall_vs_threshold(precisions, recalls, thresholds)
fpr, tpr, thresholds = roc_curve(Y_resampled,y_scores_vote_clf)
plot_roc_curve(fpr,tpr)
roc_auc_score(Y_resampled,y_scores_vote_clf)
# Try to get better recall
y_new = (y_scores_vote_clf > 0.3)
confusion_matrix(Y_resampled,y_new)
precision_score(Y_resampled, y_new)
recall_score(Y_resampled,y_new)

#%%
# Try some predictions
bins = 25
sns.distplot(forest_clf.predict(X_test),kde=False,bins=bins,label='Random Forest')
sns.distplot(Y_test,kde=False,bins=bins,label="THE TRUTH")
plt.legend()

plt.figure()
sns.distplot(svc.predict(X_test),kde=False,bins=bins,label='SVC')
sns.distplot(Y_test,kde=False,bins=bins,label="THE TRUTH")
plt.legend()

plt.figure()
sns.distplot(vote_clf.predict(X_test),kde=False,bins=bins,label='VotingClassifier')
sns.distplot(Y_test,kde=False,bins=bins,label="THE TRUTH")
plt.legend()


























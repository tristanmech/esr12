# Function approximation test
import numpy as np
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from funcs import set_rc_params
from keras.models import Sequential
from keras.layers.core import Dense
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from keras.wrappers.scikit_learn import KerasRegressor
from keras.optimizers import adam,adadelta

set_rc_params()
#%%
# True function
x = np.linspace(0,2,500)
#y = np.sin(4*np.pi*x) * np.exp(-np.abs(2*x))
y = 2*(x**2)*np.exp(x) * np.sin(x**2) * np.cos(x**3 *np.pi)
plt.plot(y)
#%%
# Training
def base_model():
    model = Sequential()
    model.add(Dense(40, input_dim=1,kernel_initializer='he_normal', activation='tanh'))
    model.add(Dense(20, activation='tanh'))
#    optim = adam(lr=0.00001)
    optim = adadelta()
    model.add(Dense(1,input_dim=1, activation="linear"))
    model.compile(loss="mean_squared_error", optimizer=optim, metrics=["mean_squared_error","accuracy"])
    return model

model = base_model()
fit = model.fit(x,y,batch_size=4,epochs=150,verbose=1)
preds = model.predict(x)
acc = np.mean(fit.history["acc"])
mse = np.mean(fit.history["mean_squared_error"])
print(f"Accuracy: {acc}\n\nMean squared error: {mse}")
plt.plot(preds,label="NN Keras")
plt.plot(y,label="THE TRUTH")
plt.figure()
plt.plot(fit.history["loss"])
#plt.show()
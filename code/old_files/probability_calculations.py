# Compute the probability of collision based on Chan's formulation
# Maybe add the other methods as well at the end
import numpy as np
from funcs import load_positions,load_velocities,load_indexes,basis_vectors,transformation_matrix,prob_algo_part1
# Get the orbital element matrices of all the objects
# OE_stations = np.load("data/OE_stations",allow_pickle=True)
# OE_satellites = np.load("data/OE_satellites",allow_pickle=True)
# OE_debris = np.load("data/OE_debris",allow_pickle=True)

# Get the encounter arrays
# FORMAT
# en[0] = primary object name
# en[1] = secondary object satnum
# en[2] = time step that the encounter occured in
# en[3] = the distance between the objects at the time of the encounter
encounters_stations = np.load("data/encounters_stations.npy",allow_pickle=True)
encounters_satellites = np.load("data/encounters_satellites.npy",allow_pickle=True)


from funcs import more_than_one_encounters_mitigation
encounters_satellites = more_than_one_encounters_mitigation(encounters_satellites)
encounters_stations = more_than_one_encounters_mitigation(encounters_stations)
# Get the position and velocity for each object involved in an
# encounter
# Velocities - km/s
station_velocities,sat_velocities,indian_velocities,breeze_velocities,iridium_velocities,cosmos_velocities,debris_velocities = load_velocities()
# Positions - km
station_positions,sat_positions,indian_positions,breeze_positions,iridium_positions,cosmos_positions,debris_positions = load_positions()

# For both positions and velocities the arrays are (timesteps,xyz)
# Use the indexes to find the correct xyz value for each object at
# each time step
station_index,sat_index,indian_index,breeze_index,iridium_index,cosmos_index,debris_index = load_indexes()

# Get the velocity and position at the time of encounter for each object involved
# primary_object_names = np.hstack((station_index[:,2],sat_index[:,2]))
# secondary_object_satnums = debris_index[:,2]
# The first 3 collumns are the data for the primary object and the last 3
# for the secondary
satellite_encounter_positions = np.zeros((encounters_satellites.shape[0],3))
satellite_encounter_velocities = np.zeros((encounters_satellites.shape[0],3))
satellite_debris_encounter_positions = np.zeros((encounters_satellites.shape[0],3))
satellite_debris_encounter_velocities = np.zeros((encounters_satellites.shape[0],3))
for i,obj in enumerate(encounters_satellites):
#     if np.any(obj[0] == primary_object_names):
    primary_indx = sat_index[np.where(obj[0] == sat_index[:,2])[0][0],1]
    secondary_indx = debris_index[np.where(obj[1] == debris_index[:,2])[0][0],1]
    # Now we know where to look in the positions and velocities
    time_indx = obj[2]
    # Now we also know the time
    satellite_encounter_positions[i] = sat_positions[time_indx,primary_indx[0]:primary_indx[1]]
    satellite_encounter_velocities[i] = sat_velocities[time_indx,
    primary_indx[0]:primary_indx[1]]

    satellite_debris_encounter_positions[i] = debris_positions[time_indx,
    secondary_indx[0]:secondary_indx[1]]
    satellite_debris_encounter_velocities[i] = debris_velocities[time_indx,
    secondary_indx[0]:secondary_indx[1]]

station_encounter_positions = np.zeros((encounters_stations.shape[0],3))
station_encounter_velocities = np.zeros((encounters_stations.shape[0],3))
station_debris_encounter_positions = np.zeros((encounters_stations.shape[0],3))
station_debris_encounter_velocities = np.zeros((encounters_stations.shape[0],3))
for i,obj in enumerate(encounters_stations):
    primary_indx = station_index[np.where(obj[0] == station_index[:,2])[0][0],1]
    secondary_indx = debris_index[np.where(obj[1] == debris_index[:,2])[0][0],1]
    time_indx = obj[2]

    station_encounter_positions[i] = station_positions[time_indx,
    primary_indx[0]:primary_indx[1]]
    station_encounter_velocities[i] = station_velocities[time_indx,
    primary_indx[0]:primary_indx[1]]

    station_debris_encounter_positions[i] = debris_positions[time_indx,
    secondary_indx[0]:secondary_indx[1]]
    station_debris_encounter_velocities[i] = debris_velocities[time_indx,
    secondary_indx[0]:secondary_indx[1]]

#%%
##################### PROBABILITY GEOMETRIC FORMULATION ####################
# Relative velocities
# v_rel = v_sec - v_prim
rel_vel_satellites =  satellite_debris_encounter_velocities - satellite_encounter_velocities
rel_vel_stations = station_debris_encounter_velocities - station_encounter_velocities
# Relative positions
# p_rel = p_sec - p_prim
rel_pos_satellites = satellite_debris_encounter_positions - satellite_encounter_positions
rel_pos_stations = station_debris_encounter_positions - station_encounter_positions

# REMEMBER THAT EACH ENCOUNTER DEFINES A NEW ENCOUNTER COORDINATE SYSTEM
# SANITY CHECKS
# The velocities of the primary objects should only have an ez component
# The posistion of the primary objects should NOT have an ey component

# Basis vectors for each object
# Satellites
ex_satellites,ey_satellites,ez_satellites = basis_vectors(rel_vel_satellites,rel_pos_satellites)
# Stations
ex_stations,ey_stations,ez_stations = basis_vectors(rel_vel_stations,rel_pos_stations)

# Transformations
# Satellites
new_rel_vel_satellites = np.zeros(3)
new_rel_pos_satellites = np.zeros(3)
for vel_vec,pos_vec,ex,ey,ez in zip(rel_vel_satellites,rel_pos_satellites,ex_satellites,ey_satellites,ez_satellites):
    # new_rel_vel_satellites.append(transformation_matrix(ex,ey,ez,vel_vec))
    # new_rel_pos_satellites.append(transformation_matrix(ex,ey,ez,pos_vec))
    new_rel_vel_satellites = np.vstack((new_rel_vel_satellites,transformation_matrix(ex,ey,ez,vel_vec)))
    new_rel_pos_satellites = np.vstack((new_rel_pos_satellites,transformation_matrix(ex,ey,ez,pos_vec)))
# remove the padding
new_rel_vel_satellites= np.delete(new_rel_vel_satellites,np.s_[0],axis=0)
new_rel_pos_satellites= np.delete(new_rel_pos_satellites,np.s_[0],axis=0)

# Stations
new_rel_vel_stations = np.zeros(3)
new_rel_pos_stations = np.zeros(3)
for vel_vec,pos_vec,ex,ey,ez in zip(rel_vel_stations,rel_pos_stations,ex_stations,ey_stations,ez_stations):
    new_rel_vel_stations = np.vstack((new_rel_vel_stations,transformation_matrix(ex,ey,ez,vel_vec)))
    new_rel_pos_stations = np.vstack((new_rel_pos_stations,transformation_matrix(ex,ey,ez,pos_vec)))
# remove the padding
new_rel_vel_stations= np.delete(new_rel_vel_stations,np.s_[0],axis=0)
new_rel_pos_stations= np.delete(new_rel_pos_stations,np.s_[0],axis=0)
#%%
# Covariance matrix
# Based on what I read on their paper they define one covariance matrix for the
# primary objects and one for the secondary. The values they have are in m2
# so take care since we are working with km here
# Primary
Cp = np.eye(6)
Cp[0,0] = 0.01
Cp[1,1] = 0.0001
Cp[2,2] = 0.0004
Cp[3,3] = 10**(-8)
Cp[4,4] = 10**(-10)
Cp[5,5] = 4*10**(-10)
# Secondary
Cs = np.eye(6)
Cs[0,0] = 1
Cs[1,1] = 10**(-2)
Cs[2,2] = 4*(10**(-2))
Cs[3,3] = 10**(-8)
Cs[4,4] = 10**(-10)
Cs[5,5] = 4*(10**(-10))

# For now we will create an artificial cov matrix to move on
Combined_cov = []
#%%
# Algorithm from supplied paper
# Inputs: sigma_x,sigma_y,xm,ym,combined object radius R,number of terms N
theta = 0 # degrees of rotation to new coordinates. It is alway negative so I
# leave it like that and put the minus at ym like they do in the paper
r_p = 0.09 # in km
r_s = 0.01
N = 10
sigma_x = 5 # km
sigma_y = 5
R = r_p + r_s

PC = []
for i in new_rel_pos_satellites:
#xm = new_rel_pos_satellites[0,0] * np.cos(theta)
#ym = new_rel_pos_satellites[0,0] * np.sin(theta)

    xm = i[0] * np.cos(theta)
    ym = i[0] * np.sin(theta)


    c,I1,I2,I3,I4,p,phi,omega_x,omega_y = prob_algo_part1(R,sigma_x,sigma_y,xm,ym)
    
    for k in range(N-5+1):
        cc = -I1(R,p,phi,omega_y,k)*c[k] + I2(R,p,phi,omega_y,k)*c[k+1] - I3(R,p,phi,omega_x,omega_y,k)*c[k+2] + I4(R,p,phi,omega_x,omega_y,k)*c[k+3]
        # Save the new c in the original array under the previous
        c = np.vstack((c,cc))
    
    s = 0
    for k in range(N-1+1):
#        print(c[k][0])
        s = s+c[k][0]
    
#    Pc = np.exp(-p*(R**2))*s
    PC.append(np.exp(-p*(R**2))*s)

print(PC)
# Now we save the probabilities
# These will be our Y data for the neural net
np.save("data/PC",PC)
# Main body of the process
import numpy as np

##################### FAKE DATA ####################
from funcs import load_fake_data
station_positions,station_velocities,station_index,stations,sat_positions,sat_velocities,satellite_index,satellites,debris_positions,debris_velocities,debris_index,debris = load_fake_data()

#################### CLOSE ENCOUNTERS DETECTION ####################
from funcs import encounter_detection
miss_dist = 5 #km

# Stations
time_indexes_stations,primary_index_stations,secondary_index_stations,distances_stations = encounter_detection(miss_dist,station_positions,debris_positions) 
# Satellites
time_indexes_satellites,primary_index_satellites,secondary_index_satellites,distances_satellites = encounter_detection(miss_dist,sat_positions,debris_positions) 

################ OE FOR EVERY OBJECT THAT HAS BEEN FLAGGED ################ 


################ THE PROBABILITY OF COLLISION OF EVERY PAIR ################
# NEURAL NET

# SVM?
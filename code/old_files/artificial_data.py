import numpy as np
from funcs import dist_fit
from blank import artificial_orbital_elements,generate_artificial_TLE,plot_data_hists
import os
from skyfield.api import load
from tqdm import tqdm


#OE_stations = np.load("data/OE_stations.npy",allow_pickle=True)
#OE_satellites = np.load("data/OE_satellites.npy",allow_pickle=True)
#OE_debris = np.load("data/OE_debris.npy",allow_pickle=True)
# Lets try using the OE data to fit a distribution to each of them and generate 
# new data from there
#OE_sat = np.asarray(OE_satellites[:,:-1].tolist())
#OE_st = np.asarray(OE_stations[:,:-1].tolist())
#OE_d = np.asarray(OE_debris[:,:-1].tolist())

# Format:
# 0 - eccentricity
# 1 - inclination
# 2 - right ascension
# 3 - argument of perigee
# 4 - mean anomaly
# 5 - mean motion
#%%
# Create artificial data
#from funcs import dist_fit,artificial_orbital_elements
#sat_params = dist_fit(OE_sat)
#station_params = dist_fit(OE_st)
#debris_params = dist_fit(OE_d)

#size = 500
# REMEMBER THE FORMAT NOW IS IN LINES NOT COLLUMNS
#artificial_satellites = np.asarray(list(artificial_orbital_elements(sat_params,size=size)))
#artificial_stations = np.asarray(list(artificial_orbital_elements(station_params,size=size)))
#artificial_debris = np.asarray(list(artificial_orbital_elements(debris_params,size=size)))

#from funcs import plot_data_hists
#plot_data_hists(sat_params,OE_sat,size)
#plot_data_hists(station_params,OE_st,size)
#plot_data_hists(debris_params,OE_d,size)
#%%
#from funcs import generate_artificial_TLE
#import os

# Remove any old files
try:
    os.remove("FAKE_DATA/FAKE_satellites.txt")
    os.remove("FAKE_DATA/FAKE_debris.txt")
    os.remove("FAKE_DATA/FAKE_stations.txt")
except:
    pass
# Fake satellites
#for i,FAKE_OE in tqdm(enumerate(artificial_satellites.T)):
#    with open("FAKE_DATA/FAKE_satellites.txt","a+") as text_file:
#        generate_artificial_TLE(i,FAKE_OE,text_file,sat=True)
# Fake stations
for i,FAKE_OE in tqdm(enumerate(artificial_stations.T)):
    with open("FAKE_DATA/FAKE_stations.txt","a+") as text_file:
        generate_artificial_TLE(i,FAKE_OE,text_file,station=True)
# Fake debris
for i,FAKE_OE in tqdm(enumerate(artificial_debris.T)):
    with open("FAKE_DATA/FAKE_debris.txt","a+") as text_file:
        generate_artificial_TLE(i,FAKE_OE,text_file,debris=True)

#%%
# Make them ready for the other files
# We will load them and make the unique here since set() will
# scramble them every time and we wont be able to verify if it works
#from skyfield.api import load

#FAKE_satellites = load.tle("FAKE_DATA/FAKE_satellites.txt")
FAKE_stations = load.tle("FAKE_DATA/FAKE_stations.txt")
FAKE_debris = load.tle("FAKE_DATA/FAKE_debris.txt")
#FAKE_satellites = list(set(FAKE_satellites.values()))
FAKE_stations = list(set(FAKE_stations.values()))
FAKE_debris = list(set(FAKE_debris.values()))
#%%
# Probably this can be put in a loop but I dont want to fiddle
# with anything at the moment
##################### INDEXES ####################
num = 0
l = 0
FAKE_station_index = np.zeros((len(FAKE_stations),3),dtype=object)
for k in range(len(FAKE_stations)):
    FAKE_station_index[k,0] = num
    FAKE_station_index[k,1] = [l,l+3]
    FAKE_station_index[k,2] = FAKE_stations[k].name
    num += 1
    l += 3
# Satellites
#num = 0
#l = 0
#FAKE_satellite_index = np.zeros((len(FAKE_satellites),3),dtype=object)
#for k in range(len(FAKE_satellites)):
#    FAKE_satellite_index[k,0] = num
#    FAKE_satellite_index[k,1] = [l,l+3]
#    FAKE_satellite_index[k,2] = FAKE_satellites[k].name
#    num += 1
#    l += 3
# Debris
num = 0
l = 0
FAKE_debris_index = np.zeros((len(FAKE_debris),3),dtype=object)
for k in range(len(FAKE_debris)):
    FAKE_debris_index[k,0] = num
    FAKE_debris_index[k,1] = [l,l+3]
    FAKE_debris_index[k,2] = FAKE_debris[k].name
    num += 1
    l += 3
#%%
##################### POSITIONS ####################
ts = load.timescale()
hours = np.arange(0.0,24,0.02)
#days= np.arange(6,8)
time_interval = ts.utc(2019,month=6,day=15,hour=hours)
print("Calculating positions...")
# Stations
FAKE_station_positions = np.zeros((len(hours),1))
for i,station_object in tqdm(enumerate(FAKE_stations)):
    current_station_positions = station_object.at(time_interval).position.km.T # (480,3) array, all the positions of the current station in the loop
    FAKE_station_positions = np.hstack((FAKE_station_positions,current_station_positions))
# Now remove the 0s
FAKE_station_positions = np.delete(FAKE_station_positions,np.s_[0],axis=1)
# Now we have the x,y,z position for each station, horizontally stacked

# Satellites
#FAKE_sat_positions = np.zeros((len(hours),1))
#for i,sat_object in tqdm(enumerate(FAKE_satellites)):
#    current_sat_positions = sat_object.at(time_interval).position.km.T
#    FAKE_sat_positions = np.hstack((FAKE_sat_positions,current_sat_positions))
## Now remove the 0s
#FAKE_sat_positions = np.delete(FAKE_sat_positions,np.s_[0],axis=1)

#Debris
FAKE_debris_positions = np.zeros((len(hours),1))
for i,debris_object in tqdm(enumerate(FAKE_debris)):
    current_debris_positions = debris_object.at(time_interval).position.km.T
    FAKE_debris_positions = np.hstack((FAKE_debris_positions,current_debris_positions))
# Now remove the 0s
FAKE_debris_positions = np.delete(FAKE_debris_positions,np.s_[0],axis=1)
#%%
##################### VELOCITIES ####################
print("Calculating velocities...")
# Stations
FAKE_station_velocities = np.zeros((len(hours),1))
for i,station_object in tqdm(enumerate(FAKE_stations)):
    current_station_velocities = station_object.at(time_interval).velocity.km_per_s.T
    FAKE_station_velocities = np.hstack((FAKE_station_velocities,current_station_velocities))
FAKE_station_velocities = np.delete(FAKE_station_velocities,np.s_[0],axis=1)

## Satellites
#FAKE_sat_velocities = np.zeros((len(hours),1))
#for i,sat_object in tqdm(enumerate(FAKE_satellites)):
#    current_sat_velocities = sat_object.at(time_interval).velocity.km_per_s.T
#    FAKE_sat_velocities = np.hstack((FAKE_sat_velocities,current_sat_velocities))
#FAKE_sat_velocities = np.delete(FAKE_sat_velocities,np.s_[0],axis=1)

#Debris
FAKE_debris_velocities = np.zeros((len(hours),1))
for i,debris_object in tqdm(enumerate(FAKE_debris)):
    current_debris_velocities = debris_object.at(time_interval).velocity.km_per_s.T
    FAKE_debris_velocities = np.hstack((FAKE_debris_velocities,current_debris_velocities))
FAKE_debris_velocities = np.delete(FAKE_debris_velocities,np.s_[0],axis=1)
#%%
##################### SAVE ARRAYS ####################
np.save("FAKE_DATA/FAKE_stations",FAKE_stations)
np.save("FAKE_DATA/FAKE_positions/FAKE_station_positions",FAKE_station_positions)
np.save("FAKE_DATA/FAKE_velocities/FAKE_station_velocities",FAKE_station_velocities)
np.save("FAKE_DATA/FAKE_station_index",FAKE_station_index)

#np.save("FAKE_DATA/FAKE_satellites",FAKE_satellites)
#np.save("FAKE_DATA/FAKE_positions/FAKE_sat_positions",FAKE_sat_positions)
#np.save("FAKE_DATA/FAKE_velocities/FAKE_sat_velocities",FAKE_sat_velocities)
#np.save("FAKE_DATA/FAKE_satellite_index",FAKE_satellite_index)

np.save("FAKE_DATA/FAKE_debris",FAKE_debris)
np.save("FAKE_DATA/FAKE_positions/FAKE_debris_positions",FAKE_debris_positions)
np.save("FAKE_DATA/FAKE_velocities/FAKE_debris_velocities",FAKE_debris_velocities)
np.save("FAKE_DATA/FAKE_debris_index",FAKE_debris_index)
#%%
# To increase the chances of collision we will now take the fake satellites
# perturb them a bit and use the resulting elements as debris
#FAKE_satellites = np.load("FAKE_DATA/FAKE_satellites.npy",allow_pickle=True)
#ecc_satellites = np.zeros((FAKE_satellites.shape[0],1))
#inc_satellites = np.zeros((FAKE_satellites.shape[0],1))
#OMEGA_satellites = np.zeros((FAKE_satellites.shape[0],1))
#omega_satellites = np.zeros((FAKE_satellites.shape[0],1))
#mean_anomaly_satellites = np.zeros((FAKE_satellites.shape[0],1))
#mean_motion_satellites = np.ones((FAKE_satellites.shape[0],1))*3
#satellite_names = np.zeros((FAKE_satellites.shape[0],1),dtype=object)
#
#for i,obj in enumerate(FAKE_satellites):
##    ecc_satellites[i] = obj.model.ecco
#    inc_satellites[i] = np.math.degrees(obj.model.inclo)
#    OMEGA_satellites[i] = np.math.degrees(obj.model.nodeo)
##    omega_satellites[i] = np.math.degrees(obj.model.argpo)
#    mean_anomaly_satellites[i] = np.math.degrees(obj.model.mo)
#    mean_motion_satellites[i] = np.math.degrees(obj.model.no)
#    satellite_names[i] = obj.name
#
#OE_satellites_new = np.hstack((ecc_satellites,inc_satellites,OMEGA_satellites,
#                           omega_satellites,mean_anomaly_satellites,
#                           mean_motion_satellites))
#
##sat_fiddle = lambda a:np.random.normal(loc=a,scale=0.01*np.abs(a))
##
##for i,oe in enumerate(OE_satellites_new.T):
##    OE_satellites_new.T[i] = sat_fiddle(oe)
#
## try with station_params
#sat_params_new = dist_fit(OE_satellites_new)
#artificial_satellites_new = np.asarray(list(artificial_orbital_elements(station_params,size=size)))
## OE_satellites_new = np.hstack((OE_satellites_new,satellite_names))
##from funcs import plot_data_hists
##plot_data_hists(sat_params_new,OE_satellites_new,size)
#
#
#try:
#        os.remove("FAKE_DATA/FAKE_satellites_new.txt")
#except:
#        pass
#
#for i,FAKE_OE in enumerate(artificial_satellites_new.T):
##    print(i)
#    with open("FAKE_DATA/FAKE_satellites_new.txt","a+") as text_file:
#        generate_artificial_TLE(i,FAKE_OE,text_file,sat=True)
#
#FAKE_satellites_new = load.tle("FAKE_DATA/FAKE_satellites_new.txt")
#FAKE_satellites_new = list(set(FAKE_satellites_new.values()))
#num = 0
#l = 0
#FAKE_satellite_index_new = np.zeros((len(FAKE_satellites_new),3),dtype=object)
#for k in range(len(FAKE_satellites)):
#    FAKE_satellite_index_new[k,0] = num
#    FAKE_satellite_index_new[k,1] = [l,l+3]
#    FAKE_satellite_index_new[k,2] = FAKE_satellites_new[k].name
#    num += 1
#    l += 3
#
#FAKE_sat_positions_new = np.zeros((len(hours),1))
#for i,sat_object in enumerate(FAKE_satellites_new):
#    current_sat_positions = sat_object.at(time_interval).position.km.T
#    FAKE_sat_positions_new = np.hstack((FAKE_sat_positions_new,current_sat_positions))
## Now remove the 0s
#FAKE_sat_positions_new = np.delete(FAKE_sat_positions_new,np.s_[0],axis=1)
#
#FAKE_sat_velocities_new = np.zeros((len(hours),1))
#for i,sat_object in enumerate(FAKE_satellites_new):
#    current_sat_velocities = sat_object.at(time_interval).velocity.km_per_s.T
#    FAKE_sat_velocities_new = np.hstack((FAKE_sat_velocities_new,current_sat_velocities))
#FAKE_sat_velocities_new = np.delete(FAKE_sat_velocities_new,np.s_[0],axis=1)
#
#np.save("FAKE_DATA/FAKE_satellites_new",FAKE_satellites_new)
#np.save("FAKE_DATA/FAKE_positions/FAKE_sat_positions_new",FAKE_sat_positions_new)
#np.save("FAKE_DATA/FAKE_velocities/FAKE_sat_velocities_new",FAKE_sat_velocities_new)
#np.save("FAKE_DATA/FAKE_satellite_index_new",FAKE_satellite_index_new)

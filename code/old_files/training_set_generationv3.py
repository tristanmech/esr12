import numpy as np
import matplotlib.pyplot as plt
from funcs import load_fake_data,get_names,encounter_detection,more_than_one_encounters_mitigation,load_FAKE_velocities,load_FAKE_positions,load_FAKE_indexes,vel_and_pos_at_encounter,probability_of_collision,extract_OE
from skyfield.api import load
##################### FAKE DATA ####################
station_positions,station_velocities,station_index,stations,sat_positions,sat_velocities,satellite_index,satellites,debris_positions,debris_velocities,debris_index,debris,sat_positions_new,sat_velocities_new,satellite_index_new,satellites_new = load_fake_data()

#%%
#################### CLOSE ENCOUNTERS DETECTION ####################
# Their paper had 10m = 0.01 km
miss_dist = 10 #km


# Stations
time_indexes_stations,primary_index_stations,secondary_index_stations,distances_stations = encounter_detection(miss_dist,station_positions,debris_positions) 
# Satellites
time_indexes_satellites,primary_index_satellites,secondary_index_satellites,distances_satellites = encounter_detection(miss_dist,sat_positions,debris_positions) 

print(f"\n\n{len(primary_index_stations)} station-debris encounters with less than {miss_dist}km distance detected")

print(f"\n{len(primary_index_satellites)} satellite-debris encounters with less than {miss_dist}km distance detected")

#%%
################ DATA FOR EVERY OBJECT THAT HAS BEEN FLAGGED ################
# Names
names_stations = get_names(primary_index_stations,station_index)
names_stations_debris = get_names(secondary_index_stations,debris_index)
names_satellites = get_names(primary_index_satellites,satellite_index)
names_satellites_debris = get_names(secondary_index_satellites,debris_index)

# Times
ts = load.timescale()
hours = np.arange(0.0,24,0.02)
times = ts.utc(2019,month=6,day=15,hour=hours)
times_of_encounter_stations = np.asarray(time_indexes_stations)
times_of_encounter_stations = np.reshape(times_of_encounter_stations,(times_of_encounter_stations.shape[0]))

times_of_encounter_satellites = np.asarray(time_indexes_satellites)
times_of_encounter_satellites = np.reshape(times_of_encounter_satellites,(times_of_encounter_satellites.shape[0]))

# Distances at the time of encounter
distances_stations = np.asarray(distances_stations)
distances_satellites = np.asarray(distances_satellites)
#distances_new = np.asarray(distances_satellites_new)

dists_stations = []
for i,j in enumerate(time_indexes_stations):
    dists_stations.append(distances_stations[i,j])
dists_stations = np.asarray(dists_stations)
dists_stations = np.reshape(dists_stations,(dists_stations.shape[0]))

dists_satellites = []
for i,j in enumerate(time_indexes_satellites):
    dists_satellites.append(distances_satellites[i,j])
dists_satellites = np.asarray(dists_satellites)
dists_satellites = np.reshape(dists_satellites,(dists_satellites.shape[0]))

# Assembly of the encounter arrays
encounters_stations = np.array([names_stations,names_stations_debris,times_of_encounter_stations,dists_stations]).T

encounters_satellites = np.array([names_satellites,names_satellites_debris,times_of_encounter_satellites,dists_satellites]).T

# Unravel the cases with more than one encounter
encounters_satellites = more_than_one_encounters_mitigation(encounters_satellites)
encounters_stations = more_than_one_encounters_mitigation(encounters_stations)
#%%
################ THE PROBABILITY OF COLLISION OF EVERY PAIR ################
# Get the position and velocity for each object involved in an
# encounter
# Velocities - km/s
FAKE_station_velocities,FAKE_sat_velocities,FAKE_debris_velocities,FAKE_sat_velocities_new = load_FAKE_velocities()
# Positions - km
FAKE_station_positions,FAKE_sat_positions,FAKE_debris_positions,FAKE_sat_positions_new = load_FAKE_positions()

# For both positions and velocities the arrays are (timesteps,xyz)
# Use the indexes to find the correct xyz value for each object at
# each time step
FAKE_station_index,FAKE_satellite_index,FAKE_debris_index,FAKE_satellite_index_new = load_FAKE_indexes()

# Get the velocity and position at the time of encounter for each object involved

sat_encounter_positions,sat_encounter_velocities,sat_debris_encounter_positions,sat_debris_encounter_velocities = vel_and_pos_at_encounter(encounters_satellites,FAKE_satellite_index,FAKE_debris_index,FAKE_sat_positions,FAKE_sat_velocities,FAKE_debris_positions,FAKE_debris_velocities)

station_encounter_positions,station_encounter_velocities,station_debris_encounter_positions,station_debris_encounter_velocities = vel_and_pos_at_encounter(encounters_stations,FAKE_station_index,FAKE_debris_index,FAKE_station_positions,FAKE_station_velocities,FAKE_debris_positions,FAKE_debris_velocities)
#%%
# Relative velocities
# v_rel = v_sec - v_prim
rel_vel_satellites =  sat_debris_encounter_velocities - sat_encounter_velocities
rel_vel_stations = station_debris_encounter_velocities - station_encounter_velocities
# Relative positions
# p_rel = p_sec - p_prim
rel_pos_satellites = sat_debris_encounter_positions - sat_encounter_positions
rel_pos_stations = station_debris_encounter_positions - station_encounter_positions

from funcs import transformation_matrix,basis_vectors
# Basis vectors for each object
# Satellites
ex_satellites,ey_satellites,ez_satellites = basis_vectors(rel_vel_satellites,rel_pos_satellites)
# Stations
ex_stations,ey_stations,ez_stations = basis_vectors(rel_vel_stations,rel_pos_stations)

# Transformations
# Satellites
new_rel_vel_satellites = np.zeros(3)
new_rel_pos_satellites = np.zeros(3)
for vel_vec,pos_vec,ex,ey,ez in zip(rel_vel_satellites,rel_pos_satellites,ex_satellites,ey_satellites,ez_satellites):
    new_rel_vel_satellites = np.vstack((new_rel_vel_satellites,transformation_matrix(ex,ey,ez,vel_vec)))
    new_rel_pos_satellites = np.vstack((new_rel_pos_satellites,transformation_matrix(ex,ey,ez,pos_vec)))
# remove the padding
new_rel_vel_satellites= np.delete(new_rel_vel_satellites,np.s_[0],axis=0)
new_rel_pos_satellites= np.delete(new_rel_pos_satellites,np.s_[0],axis=0)

# Stations
new_rel_vel_stations = np.zeros(3)
new_rel_pos_stations = np.zeros(3)
for vel_vec,pos_vec,ex,ey,ez in zip(rel_vel_stations,rel_pos_stations,ex_stations,ey_stations,ez_stations):
    new_rel_vel_stations = np.vstack((new_rel_vel_stations,transformation_matrix(ex,ey,ez,vel_vec)))
    new_rel_pos_stations = np.vstack((new_rel_pos_stations,transformation_matrix(ex,ey,ez,pos_vec)))
# remove the padding
new_rel_vel_stations= np.delete(new_rel_vel_stations,np.s_[0],axis=0)
new_rel_pos_stations= np.delete(new_rel_pos_stations,np.s_[0],axis=0)
#%%
# Actual probability calculations!
# Algorithm from supplied paper
# Inputs: sigma_x,sigma_y,xm,ym,combined object radius R,number of terms N
theta = 0 # degrees of rotation to new coordinates. It is alway negative so I
# leave it like that and put the minus at ym like they do in the paper
r_p_sat = 9 # in km
r_s_sat = 1
R = r_p_sat + r_s_sat
r_p_station = 9 # in km
r_s_station = 1
R_station = r_p_station + r_s_station

N = 20
sigma_x = 10 # km
sigma_y = sigma_x - 1 #keep it lower than sx so that no rotation is needed

PC_sat = probability_of_collision(N,R,sigma_x,sigma_y,theta,new_rel_pos_satellites)
PC_stations = probability_of_collision(N,R_station,sigma_x,sigma_y,theta,new_rel_pos_stations)

PC_sat = np.asarray(PC_sat)
PC_stations = np.asarray(PC_stations)
#%%
################ SAVE OE PAIR AND PROBABILITY (X_train,Y_train) ################
# OE of objects involved in the encounters (X_train)
FAKE_stations = np.load("FAKE_DATA/FAKE_stations.npy",allow_pickle=True)
FAKE_satellites = np.load("FAKE_DATA/FAKE_satellites.npy",allow_pickle=True)
FAKE_debris = np.load("FAKE_DATA/FAKE_debris.npy",allow_pickle=True)
FAKE_satellites_new = np.load("FAKE_DATA/FAKE_satellites_new.npy",allow_pickle=True)

OE_stations_pr,OE_stations_sec = extract_OE(encounters_stations,FAKE_stations,FAKE_debris)
OE_sat_pr,OE_sat_sec = extract_OE(encounters_satellites,FAKE_satellites,FAKE_debris)
OE_stations_pr = OE_stations_pr.astype(float)
OE_stations_sec = OE_stations_sec.astype(float)
OE_sat_pr = OE_sat_pr.astype(float)
OE_sat_sec = OE_sat_sec.astype(float)
###
# Subtrack them to get something that can fit in a machine learning
# algorithm
ecc_train_stations = np.abs(OE_stations_pr.T[0] - OE_stations_sec.T[0])
inc_train_stations = np.abs(OE_stations_pr.T[1] - OE_stations_sec.T[1])
OMEGA_train_stations = np.abs(OE_stations_pr.T[2] - OE_stations_sec.T[2])
omega_train_stations = np.abs(OE_stations_pr.T[3] - OE_stations_sec.T[3])
mean_anomaly_train_stations = np.abs(OE_stations_pr.T[4] - OE_stations_sec.T[4])
mean_motion_train_stations = np.abs(OE_stations_pr.T[5] - OE_stations_sec.T[5])

ecc_train_sat = np.abs(OE_sat_pr.T[0] - OE_sat_sec.T[0])
inc_train_sat = np.abs(OE_sat_pr.T[1] - OE_sat_sec.T[1])
OMEGA_train_sat = np.abs(OE_sat_pr.T[2] - OE_sat_sec.T[2])
omega_train_sat = np.abs(OE_sat_pr.T[3] - OE_sat_sec.T[3])
mean_anomaly_train_sat = np.abs(OE_sat_pr.T[4] - OE_sat_sec.T[4])
mean_motion_train_sat = np.abs(OE_sat_pr.T[5] - OE_sat_sec.T[5])
###
X_train_st = np.column_stack((ecc_train_stations.T,inc_train_stations.T,OMEGA_train_stations.T,omega_train_stations.T,mean_anomaly_train_stations.T,mean_motion_train_stations.T))

X_train_sat = np.column_stack((ecc_train_sat.T,inc_train_sat.T,OMEGA_train_sat.T,omega_train_sat.T,mean_anomaly_train_sat.T,mean_motion_train_sat.T))

# OR PUT THEM SIDE BY SIDE!
ecc_train_stations2 = np.column_stack((OE_stations_pr.T[0],OE_stations_sec.T[0]))
inc_train_stations2 = np.column_stack((OE_stations_pr.T[1],OE_stations_sec.T[1]))
OMEGA_train_stations2 = np.column_stack((OE_stations_pr.T[2],OE_stations_sec.T[2]))
omega_train_stations2 = np.column_stack((OE_stations_pr.T[3],OE_stations_sec.T[3]))
mean_anomaly_train_stations2 = np.column_stack((OE_stations_pr.T[4],OE_stations_sec.T[4]))
mean_motion_train_stations2 = np.column_stack((OE_stations_pr.T[5],OE_stations_sec.T[5]))

ecc_train_sat2 = np.column_stack((OE_sat_pr.T[0],OE_sat_sec.T[0]))
inc_train_sat2 = np.column_stack((OE_sat_pr.T[1],OE_sat_sec.T[1]))
OMEGA_train_sat2 = np.column_stack((OE_sat_pr.T[2],OE_sat_sec.T[2]))
omega_train_sat2 = np.column_stack((OE_sat_pr.T[3],OE_sat_sec.T[3]))
mean_anomaly_train_sat2 = np.column_stack((OE_sat_pr.T[4],OE_sat_sec.T[4]))
mean_motion_train_sat2 = np.column_stack((OE_sat_pr.T[5],OE_sat_sec.T[5]))

X_train_st2 = np.column_stack((ecc_train_stations2,inc_train_stations2,OMEGA_train_stations2,omega_train_stations2,mean_anomaly_train_stations2,mean_motion_train_stations2))

X_train_sat2 = np.column_stack((ecc_train_sat2,inc_train_sat2,OMEGA_train_sat2,omega_train_sat2,mean_anomaly_train_sat2,mean_motion_train_sat2))

# Labels
Y_train_stations = np.round(np.asarray(PC_stations),decimals=15)
Y_train_sat = np.round(np.asarray(PC_sat),decimals=15)

#%%
np.save("AI_DATA/X_train_st",X_train_st)
np.save("AI_DATA/X_train_sat",X_train_sat)
np.save("AI_DATA/X_train_st2",X_train_st2)
np.save("AI_DATA/X_train_sat2",X_train_sat2)


np.save("AI_DATA/Y_train_stations",Y_train_stations)
np.save("AI_DATA/Y_train_sat",Y_train_sat)
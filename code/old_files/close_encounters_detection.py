##################### CLOSE ENCOUNTERS DETECTION ####################
# Check for any close encounters. If any objects come closer than 10m then
# we have a closer encounter. In this case we want the time and the id
# of the objects involved in the encounter. To do that we first have to
# cross-reference all the objects we will take under consideration
import numpy as np
from tqdm import tqdm
from skyfield.api import load
import scipy
import scipy.stats as stats
#from scipy.spatial import distance

# PRIMARY OBJECTS
station_positions = np.load("positions/station_positions.npy")
station_index = np.load("positions/station_index.npy",allow_pickle=True)
stations = np.load("data/stations.npy",allow_pickle=True)

sat_positions = np.load("positions/sat_positions.npy")
satellite_index = np.load("positions/satellite_index.npy",allow_pickle=True)
satellites = np.load("data/satellites.npy",allow_pickle=True)
# SECONDARY OBJECTS
indian_positions = np.load("positions/indian_positions.npy") # 51 objects
indian_index = np.load("positions/indian_index.npy",allow_pickle=True)

breeze_positions = np.load("positions/breeze_positions.npy") # 12 objects
breeze_index = np.load("positions/breeze_index.npy",allow_pickle=True)

iridium_positions = np.load("positions/iridium_positions.npy") # 304 objects
iridium_index = np.load("positions/iridium_index.npy",allow_pickle=True)

cosmos_positions = np.load("positions/cosmos_positions.npy") # 1020 objects
cosmos_index = np.load("positions/cosmos_index.npy",allow_pickle=True)

debris_positions = np.load("positions/debris_positions.npy") # 1386 objects
debris_index = np.load("positions/debris_index.npy",allow_pickle=True)

debris = np.load("CelesTrak_data/debris.npy",allow_pickle=True)

#%%
# Check on the ISS first, first 3 collumns
#primary_pos = station_positions[:,:3]

# The time_indexes contains the number of the row which essentially is the time
# of the encounter. The iteration list will tell us which objects had the close
# encounters, by supplied the pair k,k+3 which will # allow us to pinpoint the
# place in the debris array
miss_distance_threshold = 100 # km
space_indexes_stations = []
time_indexes_stations = []
primary_object_index_stations = []
secondary_object_index_stations = []
number = 0
distances_stations = []
# number_index_stations = [] # number of the primary
# count_index_stations = [] # number of the secondary

for k in tqdm(range(0,int(station_positions.shape[1]),3)):
    # This part of the loop iterates over the primary objects. One object is
    # selected and further down we cross-reference it with all the debris we
    # are taking into consideration
    primary_pos = station_positions[:,k:3+k]
#    print(number,k,3+k)
    cnt,i,j = 0,0,3
#    count = 0
#    pbar = tqdm(total = (debris_positions.shape[1]/3) +1)
    while cnt < debris_positions.shape[1]/3:
        dist = np.linalg.norm(primary_pos - debris_positions[:,i:j],axis=1)
#        import pdb ; pdb.set_trace()
        tr = np.any(abs(dist) <= miss_distance_threshold)
#        print(tr)
        if tr:
            where = np.where(abs(dist) <= miss_distance_threshold)
#            space_indexes.append(where[1])
            time_indexes_stations.append(where[0])
            primary_object_index_stations.append([k,k+3])
            secondary_object_index_stations.append([i,j])
            # number_index_stations.append(number)
            # count_index_stations.append(cnt)
            distances_stations.append(dist)
        cnt += 1
        i += 3
        j += 3
    number += 1
##
#%%
# Satellites
# These are kept seperate for now to decrease complexity
#miss_distance_threshold = 100 # km
space_indexes_satellites = []
time_indexes_satellites = []
primary_object_index_satellites = []
secondary_object_index_satellites = []
number = 0
# number_index_satellites = [] # number of the primary
# count_index_satellites = [] # number of the secondary
distances_satellites = []
for k in tqdm(range(0,int(sat_positions.shape[1]),3)):
    # This part of the loop iterates over the primary objects. One object is
    # selected and further down we cross-reference it with all the debris we
    # are taking into consideration
    primary_pos = sat_positions[:,k:3+k]
#    print(number,k,3+k)
    cnt,i,j = 0,0,3
#    count = 0
    while cnt < debris_positions.shape[1]/3:
        dist = np.linalg.norm(primary_pos - debris_positions[:,i:j],axis=1)
        tr = np.any(abs(dist) <= miss_distance_threshold)
#        print(tr)
        if tr:
            where = np.where(abs(dist) <= miss_distance_threshold)
#            space_indexes.append(where[1])
            time_indexes_satellites.append(where[0])
            primary_object_index_satellites.append([k,k+3])
            secondary_object_index_satellites.append([i,j])
            # number_index_satellites.append(number)
            # count_index_satellites.append(cnt)
            distances_satellites.append(dist)
        cnt += 1
        i += 3
        j += 3
    number += 1
##
#%%
print(f"\n\n{len(primary_object_index_stations)} station-debris encounters with less than {miss_distance_threshold}km distance detected")

print(f"\n{len(primary_object_index_satellites)} satellite-debris encounters with less than {miss_distance_threshold}km distance detected")
#%%
# Translate the above information to something more meaningful. Show which
# are the objects that take part in the encounter and how close they get
# with each other. To do this we will go through the indexes that have been
# created previously and output any matches. Also we would like to know the time
# this encounter happened and finally the distance between the objects at the
# time of the encounter. We will output an encounters array with the format:
# encounters[:,0]: The primary object name
# encounters[:,1]: The secondary object number
# encounters[:,2]: The time of the encounter
# encounters[:,3]: The distance of the objects during the encounter

# Station-debris pairs
#encounters_stations = np.zeros((len(primary_object_index_stations),4),dtype=object)

names_stations = []
for i,index in enumerate(primary_object_index_stations):
    for j in range(np.shape(station_index)[0]):
#        print(i,index,station_index[j,1])
        if index == station_index[j,1]:
            names_stations.append(station_index[j,2])
names_stations = np.asarray(names_stations,dtype=object)

number_stations_debris = []
for i,index in enumerate(secondary_object_index_stations):
    for j in range(np.shape(debris_index)[0]):
        if index == debris_index[j,1]:
            number_stations_debris.append(debris_index[j,2])
number_stations_debris = np.asarray(number_stations_debris,dtype=object)

# encounters_stations = np.array([names_stations,names_stations_debris]).T
# Satellite-debris pairs
#encounters_satellites = np.zeros((len(primary_object_index_satellites),4),dtype=object)

names_satellites = []
for i,index in enumerate(primary_object_index_satellites):
    for j in range(np.shape(satellite_index)[0]):
        if index == satellite_index[j,1]:
            names_satellites.append(satellite_index[j,2])
names_satellites = np.asarray(names_satellites,dtype=object)


number_satellites_debris = []
for i,index in enumerate(secondary_object_index_satellites):
    for j in range(np.shape(debris_index)[0]):
        if index == debris_index[j,1]:
            number_satellites_debris.append(debris_index[j,2])
number_satellites_debris = np.asarray(number_satellites_debris,dtype=object)

# encounters_satellites = np.array([names_satellites,names_satellites_debris]).T
#%%
# Time of encounter calculation
# Some objects have more than one encounter in the timeframe. Take it into
# account
ts = load.timescale()
hours = np.arange(0.0,24,0.05)
times = ts.utc(2019,month=6,day=15,hour=hours)
standard_times = times.tt_calendar() # Converted the times back to normal
#times_of_encounter = np.zeros(len(time_indexes_stations))

# Decimal time
#times_of_encounter_stations = []
#for i,j in enumerate(time_indexes_stations):
##    print(j)
#    times_of_encounter_stations.append(hours[j])
#
#times_of_encounter_satellites = []
#for i,j in enumerate(time_indexes_satellites):
#    times_of_encounter_satellites.append(hours[j])

# Standard time
#times_of_encounter_stations = []
#for i,j in enumerate(time_indexes_stations):
#    times_of_encounter_stations.append(str(standard_times[3][j])+":"+str(standard_times[4][j]))
#
#times_of_encounter_satellites = []
#for i,j in enumerate(time_indexes_satellites):
#    times_of_encounter_satellites.append(str(standard_times[3][j])+":"+str(standard_times[4][j]))

# I think the most useful time parameter to save is the index so lets save that
# and to save me some changes and spare me the trouble of writing stuff twice
times_of_encounter_stations = np.asarray(time_indexes_stations)
times_of_encounter_stations = np.reshape(times_of_encounter_stations,(times_of_encounter_stations.shape[0]))

times_of_encounter_satellites = np.asarray(time_indexes_satellites)
times_of_encounter_satellites = np.reshape(times_of_encounter_satellites,(times_of_encounter_satellites.shape[0]))
#%%
distances_stations = np.asarray(distances_stations)
distances_satellites = np.asarray(distances_satellites)

# distances_stations and satellites are (20,480)
dists_stations = []
for i,j in enumerate(time_indexes_stations):
    dists_stations.append(distances_stations[i,j])
dists_stations = np.asarray(dists_stations)

dists_satellites = []
for i,j in enumerate(time_indexes_satellites):
    dists_satellites.append(distances_satellites[i,j])
dists_satellites = np.asarray(dists_satellites)
dists_satellites = np.reshape(dists_satellites,(dists_satellites.shape[0]))

encounters_stations = np.array([names_stations,number_stations_debris,times_of_encounter_stations,dists_stations]).T
encounters_satellites = np.array([names_satellites,number_satellites_debris,times_of_encounter_satellites,dists_satellites]).T
#%%
print("Stations-debris encounters")
print("--------------------------")
for i in range(encounters_stations.shape[0]):
    print(f"\nStation name: {encounters_stations[i,0]}")
    print(f"Debris number: {encounters_stations[i,1]}")
    print(f"Time indexes of encounter: {encounters_stations[i,2]}")
    print(f"Distance during encounter: {encounters_stations[i,3]} km")
#%%
print("Satellites-debris encounters")
print("--------------------------")
for i in range(encounters_satellites.shape[0]):
    print(f"\nStation name: {encounters_satellites[i,0]}")
    print(f"Debris number: {encounters_satellites[i,1]}")
    print(f"Time indexes of encounter: {encounters_satellites[i,2]}")
    print(f"Distance during encounter: {encounters_satellites[i,3]} km")
#%%
##################### SAVE FOR LATER USE ####################
# Now we save the encounter arrays to use them to determine the probability
# of collision

np.save("data/encounters_stations",encounters_stations)
np.save("data/encounters_satellites",encounters_satellites)






























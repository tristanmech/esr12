import numpy as np

def generate_OE(size):
    size = size
    inc_range = [80,89]
    OMEGA_range = [0,180]
    mean_anomaly_range = [0,360]
    
    ecc = np.zeros(size)
    inc = (90-80)*np.random.random_sample(size) + 80
    OMEGA = (180)*np.random.random_sample(size) + 0
    omega = np.zeros(size)
    mean_anomaly = 360*np.random.random_sample(size) + 0
    mean_motion = np.ones(size)*2
    return np.vstack((ecc,inc,OMEGA,omega,mean_anomaly,mean_motion))

size = 500

artificial_stations = generate_OE(size)
artificial_debris = generate_OE(size)
artificial_satellites = generate_OE(size)
#%%
#artificial_satellites_new = generate_OE(size)
#
#try:
#        os.remove("FAKE_DATA/FAKE_satellites_new.txt")
#except:
#        pass
#
#for i,FAKE_OE in enumerate(artificial_satellites_new.T):
##    print(i)
#    with open("FAKE_DATA/FAKE_satellites_new.txt","a+") as text_file:
#        generate_artificial_TLE(i,FAKE_OE,text_file,sat=True)
#
#FAKE_satellites_new = load.tle("FAKE_DATA/FAKE_satellites_new.txt")
#FAKE_satellites_new = list(set(FAKE_satellites_new.values()))
#num = 0
#l = 0
#FAKE_satellite_index_new = np.zeros((len(FAKE_satellites_new),3),dtype=object)
#for k in range(len(FAKE_satellites_new)):
#    FAKE_satellite_index_new[k,0] = num
#    FAKE_satellite_index_new[k,1] = [l,l+3]
#    FAKE_satellite_index_new[k,2] = FAKE_satellites_new[k].name
#    num += 1
#    l += 3
#
#FAKE_sat_positions_new = np.zeros((len(hours),1))
#for i,sat_object in enumerate(FAKE_satellites_new):
#    current_sat_positions = sat_object.at(time_interval).position.km.T
#    FAKE_sat_positions_new = np.hstack((FAKE_sat_positions_new,current_sat_positions))
## Now remove the 0s
#FAKE_sat_positions_new = np.delete(FAKE_sat_positions_new,np.s_[0],axis=1)
#
#FAKE_sat_velocities_new = np.zeros((len(hours),1))
#for i,sat_object in enumerate(FAKE_satellites_new):
#    current_sat_velocities = sat_object.at(time_interval).velocity.km_per_s.T
#    FAKE_sat_velocities_new = np.hstack((FAKE_sat_velocities_new,current_sat_velocities))
#FAKE_sat_velocities_new = np.delete(FAKE_sat_velocities_new,np.s_[0],axis=1)
#
#np.save("FAKE_DATA/FAKE_satellites_new",FAKE_satellites_new)
#np.save("FAKE_DATA/FAKE_positions/FAKE_sat_positions_new",FAKE_sat_positions_new)
#np.save("FAKE_DATA/FAKE_velocities/FAKE_sat_velocities_new",FAKE_sat_velocities_new)
#np.save("FAKE_DATA/FAKE_satellite_index_new",FAKE_satellite_index_new)
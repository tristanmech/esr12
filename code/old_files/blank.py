import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import cython

def encounter_detection(miss_dist,primary_positions,secondary_positions):
    """
    Iterate over primary and secondary pairs and detect possible encounters
    inputs: miss distance,primary positions,secondary positions
    (float,array,array)
    outputs: time_indexes,primary_index,secondary_index,distances
    (list,list,list,list)
    """
    time_indexes = []
    primary_index = []
    secondary_index = []
    number = 0
    distances = []
    
    for k in tqdm(range(0,int(primary_positions.shape[1]),3)):
        # This part of the loop iterates over the primary objects. One object is
        # selected and further down we cross-reference it with all the debris we
        # are taking into consideration
        primary_pos = primary_positions[:,k:3+k]
        cnt,i,j = 0,0,3
        # pbar = tqdm(total = (secondary_positions.shape[1]/3) +1)
        while cnt < secondary_positions.shape[1]/3:
            dist = np.linalg.norm(primary_pos - secondary_positions[:,i:j],axis=1)
            tr = np.any(abs(dist) <= miss_dist)
            if tr:
                where = np.where(abs(dist) <= miss_dist)
                time_indexes.append(where[0])
                primary_index.append([k,k+3])
                secondary_index.append([i,j])
                distances.append(dist)
            cnt += 1
            i += 3
            j += 3
            # pbar.update(1)
        number += 1
        # pbar.close()
    return time_indexes,primary_index,secondary_index,distances



def artificial_orbital_elements(parameters_list,size):
    """
    Input the parameters of the distributions and the desired size
    and get back artifially generated data
    """
#    ecc_params = parameters_list[0]
    inc_params = parameters_list[1]
    OMEGA_params = parameters_list[2]
    omega_params = parameters_list[3]
    mean_ano_params = parameters_list[4]
    mean_motion_params = parameters_list[5]

    eccentricity = np.zeros(size)
    inclination = stats.beta.rvs(*inc_params,size=size)
    OMEGA = stats.uniform.rvs(*OMEGA_params,size=size)
#    omega = stats.beta.rvs(*omega_params,size=size)
    omega = np.zeros(size)
    mean_anomaly = stats.gamma.rvs(*mean_ano_params,size=size)
    mean_motion = stats.beta.rvs(*mean_motion_params,size=size)

    return eccentricity,inclination,OMEGA,omega,mean_anomaly,mean_motion

def generate_artificial_TLE(num,FAKE_OE,text_file,sat=False,station=False,debris=False):
    """
    Given the artificial (and maybe nonsensical but its ok in this case)
    orbital elements, create a TLE file to do all the other stuff
    line0: FAKE+num
    line1: keep everything zero
    line2: artifical orbital data
    If you want to generate debris set secondary to True
    FAKE_OE must be an array with dims (1,6) or (6,)
    """
    if sat:
        line0 = "FAKE_SAT-"+str(num)+(24-len("FAKE_SAT-"+str(num)))*" "
    elif station:
        line0 = "FAKE_STATION-"+str(num)+(24-len("FAKE_STATION-"+str(num)))*" "
    elif debris:
        line0 = "FAKE_DEBRIS-"+str(num)+(24-len("FAKE_DEBRIS-"+str(num)))*" "
    
    line1 = "1 000000 64063CFF 19165.91789448 -.00000203 -00000-0 -20827-3 0 09997"
    fake_satnum = "00000"
    fake_inc = FAKE_OE[1]
    # Thought of a better way for these but lets not bother them
    indx = str(fake_inc).split(".")
    if len(indx[0]) == 1:
        fake_inc = "  "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 2:
        fake_inc = " "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 3:
        fake_inc = indx[0]+"."+indx[1][:4]
#    import pdb ; pdb.set_trace()
    fake_OMEGA = FAKE_OE[2]
    indx = str(fake_OMEGA).split(".")
    if len(indx[0]) == 1:
        fake_OMEGA = "  "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 2:
        fake_OMEGA = " "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 3:
        fake_OMEGA = indx[0]+"."+indx[1][:4]
#    fake_OMEGA = "000.0000"
#    old_ecc = f"{FAKE_OE[0]:.6f}"
#    fake_ecc = old_ecc.replace(".","")
#    if fake_ecc[0] == "-":
#        fake_ecc = fake_ecc[1:]
#    if fake_ecc[0] == "1":
#        fake_ecc = "0102481"
    fake_ecc = "0000000"
    fake_omega = FAKE_OE[3]
    indx = str(fake_omega).split(".")
    if len(indx[0]) == 1:
        fake_omega = "  "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 2:
        fake_omega = " "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 3:
        fake_omega = indx[0]+"."+indx[1][:4]
    fake_omega = "000.0000"
    fake_mean_anomaly = FAKE_OE[4]
    indx = str(fake_mean_anomaly).split(".")
    if len(indx[0]) == 1:
        fake_mean_anomaly = "  "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 2:
        fake_mean_anomaly = " "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 3:
        fake_mean_anomaly = indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 4:
        # This means we generated a negative value which
        # does not make any sense so we leave it out
        #import pdb ; pdb.set_trace()
        fake_mean_anomaly = indx[0][1:]+"."+indx[1][:4]
    if FAKE_OE[5] < 1:
        FAKE_OE[5] += 1
        fake_mean_motion = f"{FAKE_OE[5]:.6f}"
    elif FAKE_OE[5] > 1:
        fake_mean_motion = " "+f"{FAKE_OE[5]:.6f}"
    if len(fake_mean_motion) < 11:
        num = 11 - len(str(fake_mean_motion))
        fake_mean_motion = str(fake_mean_motion)+num*"0"

    # with open("data/FAKE_satellites.txt","w") as text_file:
    print(f"{line0}",file=text_file)
    print(f"{line1}",file=text_file)
    print(f"2 {fake_satnum} {fake_inc} {fake_OMEGA} {fake_ecc} {fake_omega} {fake_mean_anomaly} {fake_mean_motion}000000",file=text_file)
    
def plot_data_hists(parameters_list,data_array,size):
    """
    Helper to plot the distributions fitted for the data
    """
#    ecc_params = parameters_list[0]
    inc_params = parameters_list[1]
    OMEGA_params = parameters_list[2]
#    omega_params = parameters_list[3]
    mean_ano_params = parameters_list[4]
    mean_motion_params = parameters_list[5]


    sns.distplot(stats.beta.rvs(*inc_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,1],label="Original data")
    plt.title("Incilination")
    plt.legend()
    plt.figure()
    sns.distplot(stats.uniform.rvs(*OMEGA_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,2],label="Original data")
    plt.title("Right Ascension")
    plt.legend()
    plt.figure()
    sns.distplot(stats.gamma.rvs(*mean_ano_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,4],label="Original data")
    plt.title("Mean anomaly")
    plt.legend()
    plt.figure()
    sns.distplot(stats.beta.rvs(*mean_motion_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,5],label="Original data")
    plt.title("Mean motion")
    plt.legend()

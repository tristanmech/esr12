import numpy as np
import itertools

inc_range = np.linspace(80,90,20)
OMEGA_range = np.linspace(0,180,20)
mean_anomaly_range = np.linspace(0,360,20)

combs = itertools.product(inc_range,OMEGA_range,mean_anomaly_range)
combinations = np.array(list(combs))

from sklearn.utils import shuffle

inc = combinations.T[0]
OMEGA = combinations.T[1]
mean_anomaly = combinations.T[2]
inc2,OMEGA2,mean_anomaly2 = shuffle(inc,OMEGA,mean_anomaly,random_state=0)
ecc = np.zeros(np.shape(inc2)[0])
omega = np.zeros(np.shape(inc2)[0])
mean_motion = np.ones(np.shape(inc2)[0])*2

artificial_stations = np.vstack((ecc[:4001],inc2[:4001],OMEGA2[:4001],omega[:4001],mean_anomaly2[:4001],mean_motion[:4001]))
artificial_debris = np.vstack((ecc[:-4001],inc2[:-4001],OMEGA2[:-4001],omega[:-4001],mean_anomaly2[:-4001],mean_motion[:-4001]))


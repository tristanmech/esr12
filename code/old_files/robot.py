# This is were the AI will live
import numpy as np
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
import matplotlib.pyplot as plt
from funcs import set_rc_params
set_rc_params()
from keras.models import Sequential
from keras.layers.core import Dense
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from keras.wrappers.scikit_learn import KerasRegressor
from keras.optimizers import adam,adadelta
#from keras.layers.normalization import BatchNormalization
#from keras.layers.convolutional import Conv2D,Conv1D
#from keras.layers.convolutional import MaxPooling2D
#from keras.layers.core import Activation
#from keras.layers.core import Flatten
#from keras.layers.core import Dropout


#X_train_stations3D = np.load("AI_DATA/X_train_stations3D.npy",allow_pickle=True)
#X_train_sat3D = np.load("AI_DATA/X_train_sat3D.npy",allow_pickle=True)
#X_train_stations2D = np.load("AI_DATA/X_train_stations2D.npy",allow_pickle=True)
#X_train_sat2D = np.load("AI_DATA/X_train_sat2D.npy",allow_pickle=True)
#X_train_fiddled2D = np.load("AI_DATA/fiddled_sats/X_train_sat2D.npy",allow_pickle=True)
#X_train_fiddled3D = np.load("AI_DATA/fiddled_sats/X_train_sat3D.npy",allow_pickle=True)

Y_train_stations = np.load("AI_DATA/Y_train_stations.npy",allow_pickle=True)
Y_train_sat = np.load("AI_DATA/Y_train_sat.npy",allow_pickle=True)
#Y_train_fiddled = np.load("AI_DATA/fiddled_sats/Y_train_sat.npy",allow_pickle=True)

#X_st = X_train_stations2D
#X_st = X_train_stations3D
#X_sat = X_train_sat2D
#X_sat = X_train_sat3D
#X_sat = X_train_sat
#X_sat = X_train_fiddled3D
#X_train_st_new = np.load("AI_DATA/X_train_st_new.npy",allow_pickle=True)
X_train_st = np.load("AI_DATA/X_train_st.npy",allow_pickle=True)
X_train_sat = np.load("AI_DATA/X_train_sat.npy",allow_pickle=True)

#Y_train_stations_new = np.load("AI_DATA/Y_train_stations_new.npy",allow_pickle=True)

#Y_st = Y_train_stations
#Y_sat = Y_train_sat
#Y_sat = Y_train_fiddled

# NEW IDEA!
# Since the neural nets require x.dim == y.dim use the DIFFERENCE OF THE OE!
test_ratio = 0.15
num_test = int(test_ratio*(X_train_sat.shape[0]+X_train_st.shape[0]))

X = np.vstack((X_train_st,X_train_sat[:-num_test,:]))
Y = np.hstack((Y_train_stations,Y_train_sat[:-num_test]))

# NORMALIZE
X_max = np.max(X,axis=0)
for i in X:
#    i[0] = i[0]/X_max[0]
    i[1] = i[1]/X_max[1]
    i[2] = i[2]/X_max[2]
#    i[3] = i[3]/X_max[3]
    i[4] = i[4]/X_max[4]
    i[5] = i[5]/X_max[5]

# Delete the ecc,omega and mean_motion collumns
X = np.delete(X,(0),axis=1)
X = np.delete(X,(2),axis=1)
X = np.delete(X,(3),axis=1)

X_test = X_train_sat[-num_test:]
X_test = np.delete(X_test,(0),axis=1)
X_test = np.delete(X_test,(2),axis=1)
X_test = np.delete(X_test,(3),axis=1)
X_max2 = np.max(X_test,axis=0)
for i in X_test:
    i[0] = i[0]/X_max2[0]
    i[1] = i[1]/X_max2[1]
    i[2] = i[2]/X_max2[2]
#    i[3] = i[3]/X_max2[3]
Y_test = Y_train_sat[-num_test:]

# Remove all the zero


#%%
# Data split
# Most of the value are close to zero but there is a ~20% that is higher than
# 0.1 and it is really messing up the predictions. Idea 1: Create two networks,
# one for low probability and one for high probability. During the test phase
# the test sets need to be split in this manner as well
threshold = 0.02

Y_high = Y[np.where(Y>threshold)]
X_high = X[np.where(Y>threshold)]

Y_low = Y[np.where(Y<=threshold)]
X_low = X[np.where(Y<=threshold)]

Y_test_high = Y_test[np.where(Y_test > threshold)]
X_test_high = X_test[np.where(Y_test > threshold)]

Y_test_low = Y_test[np.where(Y_test <= threshold)]
X_test_low = X_test[np.where(Y_test <= threshold)]


# Idea 2: Still two networks but now one is a classifier and one a regressor
# Net1 will determine if the probability of collision is less than a threshold
# Class 1: The probability is less than the threshold
# Class 0: The probability is more than the threshold --> use Net2 to see how
# much
# threshold2 = 0.0001
# zero_indx = np.where(Y<threshold2)
# one_indx = np.where(Y>=threshold2)
# zero_indx_test = np.where(Y_test<threshold2)
# one_indx_test = np.where(Y_test>=threshold2)
# # For the regressor
# X_reg = X[one_indx]
# Y_reg = Y[one_indx]
# X_reg_test = X_test[one_indx_test]
# Y_reg_test = Y_test[one_indx_test]

# # For the classifier. The Y data now will become only 0s and 1s and the X data
# # are the same
# Y[zero_indx] = 0
# Y[one_indx] = 1
# Y_test[zero_indx_test] = 0
# Y_test[one_indx_test] = 1

#%%
# SVM
#svr_rbf = SVR(kernel='rbf', C=100, gamma=0.1, epsilon=.1)
#svr_lin = SVR(kernel='linear', C=100, gamma='auto')
#svr_poly = SVR(kernel='poly', C=100, gamma='auto', degree=3, epsilon=.1,
#               coef0=1)
#
#svm1 = svr_rbf.fit(X,Y)
#svm2 = svr_poly.fit(X,Y)
#svm3 = svr_lin.fit(X,Y)
##%%
## NEURAL NETWORKS!
#nn = MLPRegressor(activation="identity",early_stopping=True,max_iter=100,learning_rate="adaptive",validation_fraction=0.1)
#nn.fit(X,Y)

#%%
# KERAS NEURAL NETWORK
from networks import base_model
seed = 1
np.random.seed(seed)

#estimator = KerasRegressor(build_fn=base_model, epochs=20, batch_size=32, verbose=0)

model_high = base_model()
model_low = base_model()
fit_high = model_high.fit(X_high, Y_high, batch_size=2, epochs=100, verbose=1)
fit_low = model_low.fit(X_low, Y_low, batch_size=16, epochs=100, verbose=1)

test_loss, test_acc = model_high.evaluate(X_test_high, Y_test_high)
print('Test accuracy:', test_acc)


preds_high = model_high.predict(X_test_high)
preds_low = model_low.predict(X_test_low)
#model.summary()

kfold = KFold(n_splits=10, random_state=seed)
#results = cross_val_score(estimator, X, Y, cv=kfold)
#acc = np.mean(fit.history["acc"])
#mse = np.mean(fit.history["mean_squared_error"])
#print(f"Accuracy: {acc}\n\nMean squared error: {mse}")
#print("Results: %.6f (%.6f) MSE" % (results.mean(), results.std()))
diff_high = preds_high - Y_test_high.reshape(-1,1)
diff_low = preds_low - Y_test_low.reshape(-1,1)

#plt.plot(nn.predict(X_test),label="NN_sk")
#plt.plot(svm1.predict(X_test),label="svm-rbf")
#plt.plot(svm2.predict(X_test),label="svm-poly")
#plt.plot(svm3.predict(X_test),label="svm-lin")


plt.plot(preds_high,label="NN Keras - High")
plt.plot(Y_test_high,label="THE HIGH TRUTH")
plt.legend()
plt.figure()
plt.plot(preds_low,label="NN Keras - Low")
plt.plot(Y_test_low,label="THE LOW TRUTH")
plt.figure()
plt.plot(fit_high.history["loss"])
plt.figure()
plt.plot(fit_low.history["loss"])
plt.show()
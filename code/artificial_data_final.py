import numpy as np
from funcs import dist_fit
#from blank import artificial_orbital_elements,generate_artificial_TLE,plot_data_hists
import os
from skyfield.api import load
from tqdm import tqdm


def generate_OE(size):
    ecc = np.zeros(size)
    inc = (90-80)*np.random.random_sample(size) + 80
    OMEGA = (180)*np.random.random_sample(size) + 0
    omega = np.zeros(size)
    mean_anomaly = 360*np.random.random_sample(size) + 0
    mean_motion = np.ones(size)*2
    return np.vstack((ecc,inc,OMEGA,omega,mean_anomaly,mean_motion))

size = 30000

artificial_stations = generate_OE(size)
artificial_debris = generate_OE(size)
artificial_satellites = generate_OE(size)

#%%
# Remove any old files
try:
    os.remove("FAKE_DATA/FAKE_satellites.txt")
    os.remove("FAKE_DATA/FAKE_debris.txt")
    os.remove("FAKE_DATA/FAKE_stations.txt")
except:
    pass

# Fake satellites
for i,FAKE_OE in tqdm(enumerate(artificial_satellites.T)):
   with open("FAKE_DATA/FAKE_satellites.txt","a+") as text_file:
       generate_artificial_TLE(i,FAKE_OE,text_file,sat=True)
# Fake stations
for i,FAKE_OE in tqdm(enumerate(artificial_stations.T)):
    with open("FAKE_DATA/FAKE_stations.txt","a+") as text_file:
        generate_artificial_TLE(i,FAKE_OE,text_file,station=True)
# Fake debris
for i,FAKE_OE in tqdm(enumerate(artificial_debris.T)):
    with open("FAKE_DATA/FAKE_debris.txt","a+") as text_file:
        generate_artificial_TLE(i,FAKE_OE,text_file,debris=True)

# Make them ready for the other files
# We will load them and make the unique here since set() will
# scramble them every time and we wont be able to verify if it works
FAKE_satellites = load.tle("FAKE_DATA/FAKE_satellites.txt")
FAKE_stations = load.tle("FAKE_DATA/FAKE_stations.txt")
FAKE_debris = load.tle("FAKE_DATA/FAKE_debris.txt")
FAKE_satellites = list(set(FAKE_satellites.values()))
FAKE_stations = list(set(FAKE_stations.values()))
FAKE_debris = list(set(FAKE_debris.values()))
#%%
##################### INDEXES ####################
num = 0
l = 0
FAKE_station_index = np.zeros((len(FAKE_stations),3),dtype=object)
for k in range(len(FAKE_stations)):
    FAKE_station_index[k,0] = num
    FAKE_station_index[k,1] = [l,l+3]
    FAKE_station_index[k,2] = FAKE_stations[k].name
    num += 1
    l += 3
# Satellites
num = 0
l = 0
FAKE_satellite_index = np.zeros((len(FAKE_satellites),3),dtype=object)
for k in range(len(FAKE_satellites)):
   FAKE_satellite_index[k,0] = num
   FAKE_satellite_index[k,1] = [l,l+3]
   FAKE_satellite_index[k,2] = FAKE_satellites[k].name
   num += 1
   l += 3
# Debris
num = 0
l = 0
FAKE_debris_index = np.zeros((len(FAKE_debris),3),dtype=object)
for k in range(len(FAKE_debris)):
    FAKE_debris_index[k,0] = num
    FAKE_debris_index[k,1] = [l,l+3]
    FAKE_debris_index[k,2] = FAKE_debris[k].name
    num += 1
    l += 3
#%%
##################### POSITIONS ####################
ts = load.timescale()
hours = np.arange(0.0,24,0.02)
#days= np.arange(6,8)
time_interval = ts.utc(2019,month=6,day=15,hour=hours)
print("\nCalculating positions...")
# Stations
FAKE_station_positions = np.zeros((len(hours),1))
for i,station_object in tqdm(enumerate(FAKE_stations)):
    current_station_positions = station_object.at(time_interval).position.km.T # (480,3) array, all the positions of the current station in the loop
    FAKE_station_positions = np.hstack((FAKE_station_positions,current_station_positions))
# Now remove the 0s
FAKE_station_positions = np.delete(FAKE_station_positions,np.s_[0],axis=1)
# Now we have the x,y,z position for each station, horizontally stacked

# Satellites
FAKE_sat_positions = np.zeros((len(hours),1))
for i,sat_object in tqdm(enumerate(FAKE_satellites)):
   current_sat_positions = sat_object.at(time_interval).position.km.T
   FAKE_sat_positions = np.hstack((FAKE_sat_positions,current_sat_positions))

FAKE_sat_positions = np.delete(FAKE_sat_positions,np.s_[0],axis=1)

#Debris
FAKE_debris_positions = np.zeros((len(hours),1))
for i,debris_object in tqdm(enumerate(FAKE_debris)):
    current_debris_positions = debris_object.at(time_interval).position.km.T
    FAKE_debris_positions = np.hstack((FAKE_debris_positions,current_debris_positions))

FAKE_debris_positions = np.delete(FAKE_debris_positions,np.s_[0],axis=1)
#%%
##################### VELOCITIES ####################
print("\nCalculating velocities...")
# Stations
FAKE_station_velocities = np.zeros((len(hours),1))
for i,station_object in tqdm(enumerate(FAKE_stations)):
    current_station_velocities = station_object.at(time_interval).velocity.km_per_s.T
    FAKE_station_velocities = np.hstack((FAKE_station_velocities,current_station_velocities))
FAKE_station_velocities = np.delete(FAKE_station_velocities,np.s_[0],axis=1)

## Satellites
FAKE_sat_velocities = np.zeros((len(hours),1))
for i,sat_object in tqdm(enumerate(FAKE_satellites)):
   current_sat_velocities = sat_object.at(time_interval).velocity.km_per_s.T
   FAKE_sat_velocities = np.hstack((FAKE_sat_velocities,current_sat_velocities))
FAKE_sat_velocities = np.delete(FAKE_sat_velocities,np.s_[0],axis=1)

#Debris
FAKE_debris_velocities = np.zeros((len(hours),1))
for i,debris_object in tqdm(enumerate(FAKE_debris)):
    current_debris_velocities = debris_object.at(time_interval).velocity.km_per_s.T
    FAKE_debris_velocities = np.hstack((FAKE_debris_velocities,current_debris_velocities))
FAKE_debris_velocities = np.delete(FAKE_debris_velocities,np.s_[0],axis=1)
#%%
##################### SAVE ARRAYS ####################
np.save("FAKE_DATA/FAKE_stations",FAKE_stations)
np.save("FAKE_DATA/FAKE_positions/FAKE_station_positions",FAKE_station_positions)
np.save("FAKE_DATA/FAKE_velocities/FAKE_station_velocities",FAKE_station_velocities)
np.save("FAKE_DATA/FAKE_station_index",FAKE_station_index)

np.save("FAKE_DATA/FAKE_satellites",FAKE_satellites)
np.save("FAKE_DATA/FAKE_positions/FAKE_sat_positions",FAKE_sat_positions)
np.save("FAKE_DATA/FAKE_velocities/FAKE_sat_velocities",FAKE_sat_velocities)
np.save("FAKE_DATA/FAKE_satellite_index",FAKE_satellite_index)

np.save("FAKE_DATA/FAKE_debris",FAKE_debris)
np.save("FAKE_DATA/FAKE_positions/FAKE_debris_positions",FAKE_debris_positions)
np.save("FAKE_DATA/FAKE_velocities/FAKE_debris_velocities",FAKE_debris_velocities)
np.save("FAKE_DATA/FAKE_debris_index",FAKE_debris_index)
import numpy as np
import seaborn as sns
import scipy.stats as stats
import matplotlib.pyplot as plt
from skyfield.api import load
from tqdm import tqdm

def load_velocities():
    station_velocities = np.load("velocities/station_velocities.npy")
    sat_velocities = np.load("velocities/sat_velocities.npy",allow_pickle=True)
    indian_velocities = np.load("velocities/indian_velocities.npy",allow_pickle=True)
    breeze_velocities = np.load("velocities/breeze_velocities.npy",allow_pickle=True)
    iridium_velocities = np.load("velocities/iridium_velocities.npy",allow_pickle=True)
    cosmos_velocities = np.load("velocities/cosmos_velocities.npy",allow_pickle=True)
    debris_velocities = np.load("velocities/debris_velocities.npy",allow_pickle=True)
    return station_velocities,sat_velocities,indian_velocities,breeze_velocities,iridium_velocities,cosmos_velocities,debris_velocities

def load_positions():
    station_positions = np.load("positions/station_positions.npy",allow_pickle=True)
    sat_positions = np.load("positions/sat_positions.npy",allow_pickle=True)
    indian_positions = np.load("positions/indian_positions.npy",allow_pickle=True)
    breeze_positions = np.load("positions/breeze_positions.npy",allow_pickle=True)
    iridium_positions = np.load("positions/iridium_positions.npy",allow_pickle=True)
    cosmos_positions = np.load("positions/cosmos_positions.npy",allow_pickle=True)
    debris_positions = np.load("positions/debris_positions.npy",allow_pickle=True)
    return station_positions,sat_positions,indian_positions,breeze_positions,iridium_positions,cosmos_positions,debris_positions

def load_indexes():
    station_index = np.load("positions/station_index.npy",allow_pickle=True)
    sat_index = np.load("positions/satellite_index.npy",allow_pickle=True)
    indian_index = np.load("positions/indian_index.npy",allow_pickle=True)
    breeze_index = np.load("positions/breeze_index.npy",allow_pickle=True)
    iridium_index = np.load("positions/iridium_index.npy",allow_pickle=True)
    cosmos_index = np.load("positions/cosmos_index.npy",allow_pickle=True)
    debris_index = np.load("positions/debris_index.npy",allow_pickle=True)
    return station_index,sat_index,indian_index,breeze_index,iridium_index,cosmos_index,debris_index

# The new coordinate system will be described in accordance with the
# paper they supplied
def basis_vectors(velocities_array,positions_array):
    """
    input: array of relative velocities (num,3),
    array of relative positions (num,3)
    output: the basis vectors
    """
    ez = velocities_array/np.linalg.norm(velocities_array)
    ey = np.cross(velocities_array,positions_array)/np.linalg.norm(np.cross(velocities_array,positions_array))
    ex = np.cross(ey,ez)
    return ez,ey,ex

def transformation_matrix(ex,ey,ez,vec):
    """
    Matrix to transform coordinates from the geocentric
    to the encounter frame
    inputs: 
    basis vectors ex,ey,ez
    vector to be transformed (position/velocity)
    outputs: 
    transformed vector in line form --> e.g [Vex,Vey,Vez]
    """
    exx = np.reshape(ex,(ex.shape[0],1))
    eyy = np.reshape(ey,(ey.shape[0],1))
    ezz = np.reshape(ez,(ez.shape[0],1))
    vec = np.reshape(vec,(vec.shape[0],1))
    A = np.hstack((exx,eyy,ezz))
    A_inv = np.linalg.inv(A)
    new_vec = np.matmul(A_inv,vec)
    return new_vec.T

def prob_algo_part1(R,sigma_x,sigma_y,xm,ym):
    """
    Probability algorithm up to just before the first for loop
    """
    p1 = lambda sigma_y:1/(2*(sigma_y**2))
    phi1 = lambda sigma_x,sigma_y:1 - ( (sigma_y**2)/(sigma_x**2) )
    omega_x1 = lambda xm,sigma_x:(xm**2)/(4*(sigma_x**4))
    omega_y1 = lambda ym,sigma_y:(ym**2)/(4*(sigma_y**4))
    a01 = lambda xm,ym,sigma_x,sigma_y: (1/(2*sigma_x*sigma_y))*np.exp((-1/2)*( ((xm**2)/(sigma_x**2)) + ((ym**2)/(sigma_y**2)) ))
    
    p = p1(sigma_y)
    phi = phi1(sigma_x,sigma_y)
    omega_x = omega_x1(xm,sigma_x)
    omega_y = omega_y1(ym,sigma_y)
    a0 = a01(xm,ym,sigma_x,sigma_y)
    
    c0 = lambda a0,R: a0 * R**2
    c1 = lambda a0,R,p,phi,omega_x,omega_y: ((a0 * R**4)/2) * (p*(phi/2 + 1) + omega_x + omega_y)
    c2 = lambda a0,R,p,phi,omega_x,omega_y: ((a0 * R**6)/6) * (((p*(phi/2 + 1) + omega_x + omega_y)**2) + (p**2)*((phi**2)/2 + 1) + 2*p*phi*omega_x)
    c3 = lambda a0,R,p,phi,omega_x,omega_y: ((a0 * R**8)/24) * (((p*(phi/2 + 1) + omega_x + omega_y)**3) + 3*(p*(phi/2 + 1) + omega_x + omega_y)*((p**2)*((phi**2)/2 + 1) + 2*p*phi*omega_x) + 2*((p**3)*((phi**3)/2 + 1) + 3*(p**2)*(phi**2)*omega_x))

    I1 = lambda R,p,phi,omega_y,k: ((R**8)*(p**3)*(phi**2)*omega_y) / ((k+2)*(k+3)*((k+4)**2)*(k+5))
    I2 = lambda R,p,phi,omega_y,k: ((R**6)*(p**2)*(phi)*(p*phi*(k+ 5/2) + 2*omega_y*(phi/2 + 1))) / ((k+3)*((k+4)**2)*(k+5))
    I3 = lambda R,p,phi,omega_x,omega_y,k: ((R**4)*p*(p*phi*(phi/2 + 1)*(2*k + 5) + phi*(2*omega_y+(3*p/2))+omega_x+omega_y)) / (((k+4)**2)*(k+5))
    I4 = lambda R,p,phi,omega_x,omega_y,k: ((R**2)*(p*(2*phi+1)*(k+3) + p*(phi/2 + 1) + omega_x + omega_y)) / ((k+4)*(k+5))

    c = np.array([[c0(a0,R)],[c1(a0,R,p,phi,omega_x,omega_y)],[c2(a0,R,p,phi,omega_x,omega_y)],[c3(a0,R,p,phi,omega_x,omega_y)]],dtype=object)

    return c,I1,I2,I3,I4,p,phi,omega_x,omega_y

def plot_data_hists(parameters_list,data_array,size):
    """
    Helper to plot the distributions fitted for the data
    """
    ecc_params = parameters_list[0]
    inc_params = parameters_list[1]
    OMEGA_params = parameters_list[2]
    omega_params = parameters_list[3]
    mean_ano_params = parameters_list[4]
    mean_motion_params = parameters_list[5]


    sns.distplot(stats.gamma.rvs(*ecc_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,0],label="Original data")
    plt.title("Eccentricity")
    plt.legend()
    plt.figure()
    sns.distplot(stats.beta.rvs(*inc_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,1],label="Original data")
    plt.title("Incilination")
    plt.legend()
    plt.figure()
    sns.distplot(stats.uniform.rvs(*OMEGA_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,2],label="Original data")
    plt.title("Right Ascension")
    plt.legend()
    plt.figure()
    sns.distplot(stats.beta.rvs(*omega_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,3],label="Original data")
    plt.title("Arguement of perigee")
    plt.legend()
    plt.figure()
    sns.distplot(stats.gamma.rvs(*mean_ano_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,4],label="Original data")
    plt.title("Mean anomaly")
    plt.legend()
    plt.figure()
    sns.distplot(stats.beta.rvs(*mean_motion_params,size=size),
    label="Generated data")
    sns.distplot(data_array[:,5],label="Original data")
    plt.title("Mean motion")
    plt.legend()

def set_rc_params():
    """
    Just making the rc params into one line
    """
    plt.rcParams['font.size'] = 21
    plt.rcParams['font.family'] = 'serif'
    plt.rcParams['font.serif'] = 'Ubuntu'
    plt.rcParams['font.monospace'] = 'Ubuntu Mono'
    plt.rcParams['figure.figsize'] = [12., 8.0]
    plt.rcParams['mathtext.fontset'] = 'cm'
    plt.rcParams['mathtext.rm'] = 'serif'

def generate_artificial_TLE(num,FAKE_OE,text_file,sat=False,station=False,debris=False):
    """
    Given the artificial (and maybe nonsensical but its ok in this case)
    orbital elements, create a TLE file to do all the other stuff
    line0: FAKE+num
    line1: keep everything zero
    line2: artifical orbital data
    If you want to generate debris set secondary to True
    FAKE_OE must be an array with dims (1,6) or (6,)
    """
    if sat:
        line0 = "FAKE_SAT-"+str(num)+(24-len("FAKE_SAT-"+str(num)))*" "
    elif station:
        line0 = "FAKE_STATION-"+str(num)+(24-len("FAKE_STATION-"+str(num)))*" "
    elif debris:
        line0 = "FAKE_DEBRIS-"+str(num)+(24-len("FAKE_DEBRIS-"+str(num)))*" "

    line1 = "1 000000 64063CFF 19165.91789448 -.00000203 -00000-0 -20827-3 0 09997"
    fake_satnum = "00000"
    fake_inc = FAKE_OE[1]
    # Thought of a better way for these but lets not bother them
    indx = str(fake_inc).split(".")
    if len(indx[0]) == 1:
        fake_inc = "  "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 2:
        fake_inc = " "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 3:
        fake_inc = indx[0]+"."+indx[1][:4]

    fake_OMEGA = FAKE_OE[2]
    indx = str(fake_OMEGA).split(".")
    if len(indx[0]) == 1:
        fake_OMEGA = "  "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 2:
        fake_OMEGA = " "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 3:
        fake_OMEGA = indx[0]+"."+indx[1][:4]

    old_ecc = f"{FAKE_OE[0]:.6f}"
    fake_ecc = old_ecc.replace(".","")
    if fake_ecc[0] == "-":
        fake_ecc = fake_ecc[1:]
    if fake_ecc[0] == "1":
        fake_ecc = "0102481"
    fake_omega = FAKE_OE[3]
    indx = str(fake_omega).split(".")
    if len(indx[0]) == 1:
        fake_omega = "  "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 2:
        fake_omega = " "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 3:
        fake_omega = indx[0]+"."+indx[1][:4]

    fake_mean_anomaly = FAKE_OE[4]
    indx = str(fake_mean_anomaly).split(".")
    if len(indx[0]) == 1:
        fake_mean_anomaly = "  "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 2:
        fake_mean_anomaly = " "+indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 3:
        fake_mean_anomaly = indx[0]+"."+indx[1][:4]
    elif len(indx[0]) == 4:
        fake_mean_anomaly = indx[0][1:]+"."+indx[1][:4]

    if FAKE_OE[5] < 1:
        FAKE_OE[5] += 1
        fake_mean_motion = f"{FAKE_OE[5]:.6f}"
    elif FAKE_OE[5] > 1:
        fake_mean_motion = " "+f"{FAKE_OE[5]:.6f}"
    if len(fake_mean_motion) < 11:
        num = 11 - len(str(fake_mean_motion))
        fake_mean_motion = str(fake_mean_motion)+num*"0"

    print(f"{line0}",file=text_file)
    print(f"{line1}",file=text_file)
    print(f"2 {fake_satnum} {fake_inc} {fake_OMEGA} {fake_ecc} {fake_omega} {fake_mean_anomaly} {fake_mean_motion}000000",file=text_file)

# Function to fit distributions to the data
def dist_fit(OE_data):
    """
    Input orbital elements from TLE data and get the parameters
    for the chosen distributions. They are:
    eccentriciy: gamma
    inclination: beta
    right ascension: uniform
    argument of perigee: beta
    mean anomaly: gamma
    mean motion: beta
    They were chosen by observing the histograms of the original data
    """
    ecc_params = np.asarray(list(stats.gamma.fit(OE_data[:,0])))
    ecc_params = np.reshape(ecc_params,(ecc_params.shape[0],1))

    inc_params = np.asarray(list(stats.beta.fit(OE_data[:,1])))
    inc_params = np.reshape(inc_params,(inc_params.shape[0],1))

    OMEGA_params = np.asarray(list(stats.uniform.fit(OE_data[:,2])))
    OMEGA_params  = np.reshape(OMEGA_params,(OMEGA_params.shape[0],1))

    omega_params = np.asarray(list(stats.beta.fit(OE_data[:,3])))
    omega_params = np.reshape(omega_params,(omega_params.shape[0],1))

    mean_ano_params = np.asarray(list(stats.gamma.fit(OE_data[:,4])))
    mean_ano_params = np.reshape(mean_ano_params,(mean_ano_params.shape[0],1))

    mean_motion_params = np.asarray(list(stats.beta.fit(OE_data[:,5])))
    mean_motion_params = np.reshape(mean_motion_params,(mean_motion_params.shape[0],1))

    param_list = [ecc_params,inc_params,OMEGA_params,omega_params,
                             mean_ano_params,mean_motion_params]
    return param_list

# Function to generate data
def artificial_orbital_elements(parameters_list,size):
    """
    Input the parameters of the distributions and the desired size
    and get back artifially generated data
    """
    ecc_params = parameters_list[0]
    inc_params = parameters_list[1]
    OMEGA_params = parameters_list[2]
    omega_params = parameters_list[3]
    mean_ano_params = parameters_list[4]
    mean_motion_params = parameters_list[5]

    eccentricity = stats.gamma.rvs(*ecc_params,size=size)
    inclination = stats.beta.rvs(*inc_params,size=size)
    OMEGA = stats.uniform.rvs(*OMEGA_params,size=size)
    omega = stats.beta.rvs(*omega_params,size=size)
    mean_anomaly = stats.gamma.rvs(*mean_ano_params,size=size)
    mean_motion = stats.beta.rvs(*mean_motion_params,size=size)

    return eccentricity,inclination,OMEGA,omega,mean_anomaly,mean_motion

def load_fake_data():
    station_positions = np.load("FAKE_DATA/FAKE_positions/FAKE_station_positions.npy")
    station_velocities = np.load("FAKE_DATA/FAKE_velocities/FAKE_station_velocities.npy")
    station_index = np.load("FAKE_DATA/FAKE_station_index.npy",allow_pickle=True)
    stations = np.load("FAKE_DATA/FAKE_stations.npy",allow_pickle=True)

    sat_positions = np.load("FAKE_DATA/FAKE_positions/FAKE_sat_positions.npy")
    sat_velocities = np.load("FAKE_DATA/FAKE_velocities/FAKE_sat_velocities.npy")
    satellite_index = np.load("FAKE_DATA/FAKE_satellite_index.npy",allow_pickle=True)
    satellites = np.load("FAKE_DATA/FAKE_satellites.npy",allow_pickle=True)

    debris_positions = np.load("FAKE_DATA/FAKE_positions/FAKE_debris_positions.npy")
    debris_velocities = np.load("FAKE_DATA/FAKE_velocities/FAKE_debris_velocities.npy")
    debris_index = np.load("FAKE_DATA/FAKE_debris_index.npy",allow_pickle=True)
    debris = np.load("FAKE_DATA/FAKE_debris.npy",allow_pickle=True)

    sat_positions_new = np.load("FAKE_DATA/FAKE_positions/FAKE_sat_positions_new.npy")
    sat_velocities_new = np.load("FAKE_DATA/FAKE_velocities/FAKE_sat_velocities_new.npy")
    satellite_index_new = np.load("FAKE_DATA/FAKE_satellite_index_new.npy",allow_pickle=True)
    satellites_new = np.load("FAKE_DATA/FAKE_satellites_new.npy",allow_pickle=True)

    return station_positions,station_velocities,station_index,stations,sat_positions,sat_velocities,satellite_index,satellites,debris_positions,debris_velocities,debris_index,debris,sat_positions_new,sat_velocities_new,satellite_index_new,satellites_new

def encounter_detection(miss_dist,primary_positions,secondary_positions):
    """
    Iterate over primary and secondary pairs and detect possible encounters
    inputs: miss distance,primary positions,secondary positions
    (float,array,array)
    outputs: time_indexes,primary_index,secondary_index,distances
    (list,list,list,list)
    """
    time_indexes = []
    primary_index = []
    secondary_index = []
    number = 0
    distances = []

    for k in tqdm(range(0,int(primary_positions.shape[1]),3)):
        # This part of the loop iterates over the primary objects. One object is
        # selected and further down we cross-reference it with all the debris we
        # are taking into consideration
        primary_pos = primary_positions[:,k:3+k]
        cnt,i,j = 0,0,3
        # pbar = tqdm(total = (secondary_positions.shape[1]/3) +1)
        while cnt < secondary_positions.shape[1]/3:
            dist = np.linalg.norm(primary_pos - secondary_positions[:,i:j],axis=1)
            tr = np.any(abs(dist) <= miss_dist)
            if tr:
                where = np.where(abs(dist) <= miss_dist)
                time_indexes.append(where[0])
                primary_index.append([k,k+3])
                secondary_index.append([i,j])
                distances.append(dist)
            cnt += 1
            i += 3
            j += 3
            # pbar.update(1)
        number += 1
        # pbar.close()
    return time_indexes,primary_index,secondary_index,distances

def get_names(object_index_encounter,object_index_memory):
    """
    Get the names of the objects involved in the encounter
    using their indexes
    inputs: object index from database, index from encounter
    outputs: array of names
    """
    names = []
    for i,index in enumerate(object_index_encounter):
        for j in range(np.shape(object_index_memory)[0]):
            if index == object_index_memory[j,1]:
                names.append(object_index_memory[j,2])
    names = np.asarray(names,dtype=object)

    return names

def load_FAKE_velocities():
    FAKE_station_velocities = np.load("FAKE_DATA/FAKE_velocities/FAKE_station_velocities.npy")
    FAKE_sat_velocities = np.load("FAKE_DATA/FAKE_velocities/FAKE_sat_velocities.npy",allow_pickle=True)
    FAKE_debris_velocities = np.load("FAKE_DATA/FAKE_velocities/FAKE_debris_velocities.npy",allow_pickle=True)
    FAKE_sat_velocities_new = np.load("FAKE_DATA/FAKE_velocities/FAKE_sat_velocities_new.npy",allow_pickle=True)
    return FAKE_station_velocities,FAKE_sat_velocities,FAKE_debris_velocities,FAKE_sat_velocities_new

def load_FAKE_positions():
    FAKE_station_positions = np.load("FAKE_DATA/FAKE_positions/FAKE_station_positions.npy")
    FAKE_sat_positions = np.load("FAKE_DATA/FAKE_positions/FAKE_sat_positions.npy",allow_pickle=True)
    FAKE_debris_positions = np.load("FAKE_DATA/FAKE_positions/FAKE_debris_positions.npy",allow_pickle=True)
    FAKE_sat_positions_new = np.load("FAKE_DATA/FAKE_positions/FAKE_sat_positions_new.npy",allow_pickle=True)
    return FAKE_station_positions,FAKE_sat_positions,FAKE_debris_positions,FAKE_sat_positions_new

def load_FAKE_indexes():
    FAKE_station_index = np.load("FAKE_DATA/FAKE_station_index.npy",allow_pickle=True)
    FAKE_satellite_index = np.load("FAKE_DATA/FAKE_satellite_index.npy",allow_pickle=True)
    FAKE_debris_index = np.load("FAKE_DATA/FAKE_debris_index.npy",allow_pickle=True)
    FAKE_satellite_index_new = np.load("FAKE_DATA/FAKE_satellite_index_new.npy",allow_pickle=True)
    return FAKE_station_index,FAKE_satellite_index,FAKE_debris_index,FAKE_satellite_index_new

def vel_and_pos_at_encounter(encounter_object,primary_object_index,debris_index,object_positions,object_velocities,debris_positions,debris_velocities):
    """
    Get the velocity and position at the time of encounter for each object involved. I think the inputs speak for themselves
    """
    object_encounter_positions = np.zeros((encounter_object.shape[0],3))
    object_encounter_velocities = np.zeros((encounter_object.shape[0],3))
    object_debris_encounter_positions = np.zeros((encounter_object.shape[0],3))
    object_debris_encounter_velocities = np.zeros((encounter_object.shape[0],3))
    for i,obj in enumerate(encounter_object):
    #     if np.any(obj[0] == primary_object_names):
#        import pdb ; pdb.set_trace()
        primary_indx = primary_object_index[np.where(obj[0] == primary_object_index[:,2])[0][0],1]
        secondary_indx = debris_index[np.where(obj[1] == debris_index[:,2])[0][0],1]
        # Now we know where to look in the positions and velocities
        time_indx = obj[2]
        # Now we also know the time
#        import pdb ; pdb.set_trace()
#        print(i)
        object_encounter_positions[i] = object_positions[time_indx,primary_indx[0]:primary_indx[1]]
#        import pdb ; pdb.set_trace()
        object_encounter_velocities[i] = object_velocities[time_indx,
        primary_indx[0]:primary_indx[1]]

        object_debris_encounter_positions[i] = debris_positions[time_indx,
        secondary_indx[0]:secondary_indx[1]]
        object_debris_encounter_velocities[i] = debris_velocities[time_indx,
        secondary_indx[0]:secondary_indx[1]]

    return object_encounter_positions,object_encounter_velocities,object_debris_encounter_positions,object_debris_encounter_velocities

def probability_of_collision(N,R,sigma_x,sigma_y,theta,new_rel_pos):
    PC = []
    for i in new_rel_pos:
        xm = i[0] * np.cos(theta)
        ym = -i[0] * np.sin(theta)
        c,I1,I2,I3,I4,p,phi,omega_x,omega_y = prob_algo_part1(R,sigma_x,sigma_y,xm,ym)
        for k in range(N-5+1):
            cc = -I1(R,p,phi,omega_y,k)*c[k] + I2(R,p,phi,omega_y,k)*c[k+1] - I3(R,p,phi,omega_x,omega_y,k)*c[k+2] + I4(R,p,phi,omega_x,omega_y,k)*c[k+3]
            # Save the new c in the original array under the previous
            c = np.vstack((c,cc))
        s = 0
        for k in range(N-1+1):
            s = s+c[k][0]
        PC.append(np.exp(-p*(R**2))*s)
    
    return PC

def more_than_one_encounters_mitigation(encounters):
    """
    If two objects have more than one encounter add them
    a the end as seperate encounters and delete the line
    containing both of them
    """
    indexes = []
    for indx,i in enumerate(encounters):
    # If two objects have more than one encounter
        if type(i[2]) == type(encounters) and len(i[2]) > 1:
            #print("Ja mann!")
            distts = i[3]
            timess = i[2]
            min_timess = timess[np.where(np.min(distts) == distts)[0]]
            ar = np.array([np.array([i[0]]),np.array([i[1]]),np.array([min_timess[0]]),np.array([np.mean(distts)])],dtype=object)
            encounters = np.row_stack((encounters,ar.T))
            indexes.append(indx)
                
            # and delete the line containing both of them
#            import pdb ; pdb.set_trace()
    encounters = np.delete(encounters,(indexes),axis=0)
    return encounters

def extract_OE(encounters,FAKE_primary_objects,FAKE_secondary_objects):
    """
    Input the encounter array and get back the OE of the primary and secondary
    objects involved in the collision.
    outputs:
    OE_x: array with each line containing the OE of an object
    shape=(num_objects,6)
    """
    a = encounters[:,0]
    b = encounters[:,1]
    c = FAKE_primary_objects
    d = FAKE_secondary_objects
    OE_prim = np.zeros(6,dtype=object)
    OE_sec = np.zeros(6,dtype=object)
    for i,j in zip(a,b):
        for pr in c:
#            import pdb ; pdb.set_trace()
            # name_prim_index = np.where(i == pr.name)[0]
#            import pdb ; pdb.set_trace()
            if pr.name == i:
                OE_prim_loop = np.array([pr.model.ecco,np.rad2deg(pr.model.inclo),np.rad2deg(pr.model.nodeo),np.rad2deg(pr.model.argpo),np.rad2deg(pr.model.mo),np.rad2deg(pr.model.no)])
                OE_prim = np.row_stack((OE_prim,OE_prim_loop))
        for sec in d:
            if sec.name ==j:
                # name_sec_index = np.where(j == sec.name)[0]
                OE_sec_loop = np.array([sec.model.ecco,np.rad2deg(sec.model.inclo),np.rad2deg(sec.model.nodeo),np.rad2deg(sec.model.argpo),np.rad2deg(sec.model.mo),np.rad2deg(sec.model.no)])
                OE_sec = np.row_stack((OE_sec,OE_sec_loop))
    
    OE_prim = np.delete(OE_prim,np.s_[0],axis=0)
    OE_sec = np.delete(OE_sec,np.s_[0],axis=0)
    return OE_prim,OE_sec

def plot_precision_recall_vs_threshold(precisions, recalls, thresholds):
    plt.plot(thresholds, precisions[:-1], "C0--", label="Precision")
    plt.plot(thresholds, recalls[:-1], "C1-", label="Recall")
    plt.xlabel("Threshold")
#    plt.legend(loc="upper left")
    plt.legend()
    plt.ylim([0, 1])

def plot_roc_curve(fpr, tpr, label=None):
    plt.plot(fpr, tpr, linewidth=2, label=label)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.axis([0, 1, 0, 1])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')

def array2bmatrix(a):
    """Returns a LaTeX bmatrix

    :a: numpy array
    :returns: LaTeX bmatrix as a string
    To get it correctly formatted print() it
    e.g 
    d = bmatrix(a)
    print(d)
    """
    if len(a.shape) > 2:
        raise ValueError('bmatrix can at most display two dimensions')
    lines = str(a).replace('[', '').replace(']', '').splitlines()
    rv = [r'\begin{bmatrix}']
    rv += ['  ' + ' & '.join(l.split()) + r'\\' for l in lines]
    rv +=  [r'\end{bmatrix}']
    return '\n'.join(rv)